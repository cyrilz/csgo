using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(AppMetricaManager))]
public class AppMetricaManagerInspector : Editor
{
    private SerializedProperty debugProperty;
    private SerializedProperty eventsCountProperty;
    private SerializedProperty eventNamesProperty;
    private SerializedProperty eventParametersProperty;
    
    private void OnEnable()
    {
        eventsCountProperty = serializedObject.FindProperty("eventsCount");
        eventNamesProperty = serializedObject.FindProperty("eventNames");
        eventParametersProperty = serializedObject.FindProperty("eventParameters");
        debugProperty = serializedObject.FindProperty("debug");
    }
    
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(debugProperty);
        EditorGUILayout.PropertyField(eventsCountProperty);
        
        if (eventNamesProperty.arraySize != eventsCountProperty.intValue)
            eventNamesProperty.arraySize = eventsCountProperty.intValue;
        
        if (eventParametersProperty.arraySize != eventsCountProperty.intValue)
            eventParametersProperty.arraySize = eventsCountProperty.intValue;
        
        for (int i = 0; i < eventsCountProperty.intValue; i++)
        {
            EditorGUILayout.Space(10);
            EditorGUILayout.PropertyField(eventNamesProperty.GetArrayElementAtIndex(i), new GUIContent("Event Name"));
            EditorGUILayout.PropertyField(eventParametersProperty.GetArrayElementAtIndex(i), new GUIContent("Event Parameters"));
        }
        
        serializedObject.ApplyModifiedProperties();
    }
}