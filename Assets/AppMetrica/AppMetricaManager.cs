﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;

public class AppMetricaManager : MonoBehaviour
{
    public static AppMetricaManager instance;
    public bool debug;
    public int eventsCount;
    public string[] eventNames = new string[0];
    public string[] eventParameters = new string[0];

    private void Awake()
    {
        if (instance == null) 
        { 
            instance = this; 
            DontDestroyOnLoad(gameObject);
        } 
        else 
        { 
            Destroy(gameObject); 
        }
    }

    public void SendEvent(string name, params object[] parameters)
    {
        SendEvent(name, false, parameters);
    }

    public void SendEvent(string name, bool force, params object[] parameters)
    {
        int indexOf = eventNames.ToList().IndexOf(name);
        
        if (indexOf == -1)
        {
            Debug.LogError("AppMetricaManager: Event with name '" + name + "' dont exists");
            return;
        }

        List<string> paramsNames = eventParameters[indexOf].Split(' ').Where(str => !string.IsNullOrWhiteSpace(str)).ToList();
        if (parameters.Length != paramsNames.Count)
        {
            Debug.LogError("AppMetricaManager: Event with name '" + name + "' dont have " + eventParameters[indexOf].Length + " parameters");
            return;
        }

        Dictionary<string, object> toSend = new Dictionary<string, object>();
        for (int i = 0; i < parameters.Length; i++)
            toSend.Add(paramsNames[i], parameters[i]);
        
        AppMetrica.Instance.ReportEvent(name, toSend);

        if (debug)
        {
            string log = $"Send '{name}' event \n";
            for (int i = 0; i < toSend.Count; i++)
                log += $"{paramsNames[i]}: {parameters[i]}\n";
            Debug.Log(log);
        }

        if (force)
            AppMetrica.Instance.SendEventsBuffer();
    }

    public bool CheckConnection()
    {
        return Application.internetReachability != NetworkReachability.NotReachable;
    }
}
