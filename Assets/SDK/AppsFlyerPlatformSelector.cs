﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppsFlyerPlatformSelector : MonoBehaviour
{
    [SerializeField] private GameObject androidObject;
    [SerializeField] private GameObject iosObject;

    private void Awake()
    {
#if UNITY_IOS || UNITY_IPHONE
        androidObject.SetActive(false);
#else        
        iosObject.SetActive(false);
#endif
    }
}
