﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonDisabler : MonoBehaviour
{
    [SerializeField] private bool UnlockAfterInterstitial;
    [SerializeField] private bool UnlockAfterRewarded;
    [SerializeField] private Button[] buttonsToLock;
    
    private Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(DisableButtons);
    }

    private void Start()
    {
        if (UnlockAfterInterstitial)
            AppLovinManager.instance.Interstitial.OnTotalyClosed.Subscribe(OnEnable);
        
        if (UnlockAfterRewarded)
            AppLovinManager.instance.Rewarded.OnTotalyClosed.Subscribe(OnEnable);
    }

    public void DisableButtons()
    {
        foreach (Button button in buttonsToLock)
            button.interactable = false;
    }

    private void OnEnable()
    {
        foreach (Button button in buttonsToLock)
            button.interactable = true;
    }

    private void OnDestroy()
    {
        if (UnlockAfterInterstitial && AppLovinManager.instance)
            AppLovinManager.instance.Interstitial.OnTotalyClosed.Unsubscribe(OnEnable);
        
        if (UnlockAfterRewarded && AppLovinManager.instance)
            AppLovinManager.instance.Rewarded.OnTotalyClosed.Unsubscribe(OnEnable);
    }
}