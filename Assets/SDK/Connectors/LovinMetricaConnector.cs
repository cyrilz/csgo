﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LovinMetricaConnector : MonoBehaviour
{
    public static LovinMetricaConnector instance;
    
    [SerializeField] private AppMetricaManager metrica;
    [SerializeField] private AppLovinManager lovin;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(gameObject);
        
        lovin.Banner.OnShow.Subscribe(() => metrica.SendEvent("video_ads_watch", "banner", "bottom", "watched", AppMetricaManager.instance.CheckConnection()));
        
        lovin.Interstitial.OnDismissed.Subscribe(() => metrica.SendEvent("video_ads_watch", "interstitial", "level_start", "watched", AppMetricaManager.instance.CheckConnection()));
        lovin.Interstitial.OnNotReady.Subscribe(() => metrica.SendEvent("video_ads_available", "interstitial", "level_start", "not_available", AppMetricaManager.instance.CheckConnection()));
        lovin.Interstitial.OnReady.Subscribe(() => metrica.SendEvent("video_ads_available", "interstitial", "level_start", "success", AppMetricaManager.instance.CheckConnection()));
        lovin.Interstitial.OnDisplayed.Subscribe(() => metrica.SendEvent("video_ads_started", "interstitial", "level_start", "start", AppMetricaManager.instance.CheckConnection()));
        lovin.Interstitial.OnFailedToDisplay.Subscribe(() => metrica.SendEvent("video_ads_started", "interstitial", "level_start", "canceled", AppMetricaManager.instance.CheckConnection()));
        
        lovin.Rewarded.OnReady.Subscribe(placement => metrica.SendEvent("video_ads_available", "rewarded", placement, "success", AppMetricaManager.instance.CheckConnection()));
        lovin.Rewarded.OnNotReady.Subscribe(placement => metrica.SendEvent("video_ads_available", "rewarded", placement, "not_available", AppMetricaManager.instance.CheckConnection()));
        lovin.Rewarded.OnFailedToDisplay.Subscribe(placement => metrica.SendEvent("video_ads_started", "rewarded", placement, "canceled", AppMetricaManager.instance.CheckConnection()));
        lovin.Rewarded.OnDisplayed.Subscribe(placement => metrica.SendEvent("video_ads_started", "rewarded", placement, "start", AppMetricaManager.instance.CheckConnection()));
        lovin.Rewarded.OnDismissed.Subscribe(args => metrica.SendEvent("video_ads_watch", "rewarded", args.placement, args.result, AppMetricaManager.instance.CheckConnection()));
    }
}
