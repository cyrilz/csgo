using Menus;
using UnityEngine;

public class GDPRButton : MonoBehaviour
{
    public void OnClick()
    {
        GUIManager.Open<GDPRMenu>(true);
    }
}