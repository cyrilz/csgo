using Menus;
using UnityEngine;

public class GDPRMenu : UIMenu
{
    public void OnLinkClick()
    {
        Application.OpenURL("https://privacy.azurgames.com/");
    }

    public void OnAcceptClick()
    {
        MaxSdk.SetHasUserConsent(true);

        GUIManager.Close<GDPRMenu>();
    }
}