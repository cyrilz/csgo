using System;
using Menus;
using UnityEngine;

// Должен лежать в global, а не в SDK
public class GDPRManager : MonoBehaviour
{
    [SerializeField] private GameObject GDPRButton;
    
    private static bool inEurope;
    private static bool isChecked;

    private void Awake()
    {
        if (!isChecked)
            MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
            {
                if (sdkConfiguration.ConsentDialogState == MaxSdk.ConsentDialogState.Applies) { 
                    if (!MaxSdk.HasUserConsent()) 
                        GUIManager.Open<GDPRMenu>(true);
    
                    GDPRButton.SetActive(true);
                    inEurope = true;
                }
    
                isChecked = true;
            };
        
        if (inEurope)
            GDPRButton.SetActive(true);
    }
}