﻿// Компонент должен лежать в global. Вызывает рекламу интерстишиала при перезапуске сцены
public class AdShowManager : Singleton<AdShowManager>
{
    private static bool isNotFirstLaunch;
    
    private void Start()
    {
        if (isNotFirstLaunch)
            AppLovinManager.instance.Interstitial.Show(null);
        
        isNotFirstLaunch = true;
    }
}