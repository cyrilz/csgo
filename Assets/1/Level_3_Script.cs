using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Level_3_Script : Level
{
    
    [SerializeField]
    private GameObject weapon;
    [SerializeField]
    private GameObject scissors;

    [SerializeField]
    private GameObject bomb;


    [SerializeField]
    private Transform playerFinalPosition;

    NavMeshAgent playerNavMeshAgent;
    Bomb bombScript;
    Hands hands;

    private void Awake()
    {
        playerNavMeshAgent = player.GetComponent<NavMeshAgent>();
        hands = player.GetComponent<Hands>();
    }
    public void Activate()
    {
        playerNavMeshAgent.enabled = true;
        playerNavMeshAgent.speed = 9;
        playerNavMeshAgent.destination = playerFinalPosition.position;
    }



    // Start is called before the first frame update
    private void Start()
    {
        EventManager.OnStartGame.Subscribe(Activate);

        playerNavMeshAgent.enabled = false;
        player.transform.position = playerStartPosition.position;
        player.transform.rotation = playerStartPosition.rotation;

        player.GetComponent<Hands>().SetNewWeapon(weapon);
    }

    // Update is called once per frame
    void Update()
    {
        if (playerNavMeshAgent.enabled == true)
        {
            //Debug.Log(Vector3.Distance(player.transform.position, playerFinalPosition.position));
            if (Vector3.Distance(player.transform.position, playerFinalPosition.position) < 1f)
            {
                player.transform.position = Vector3.MoveTowards(player.transform.position, playerFinalPosition.position, 0.7f);// playerFinalPosition.position;
                player.transform.rotation = Quaternion.Euler(Vector3.MoveTowards(player.transform.rotation.eulerAngles, playerFinalPosition.rotation.eulerAngles, 0.7f));// playerFinalPosition.position;
                                                                                                                                                                  //player.transform.rotation = playerFinalPosition.rotation;
                Invoke("StartBombDeactivation", 0.5f);
                playerNavMeshAgent.enabled = false;



            }
        }
        
    }

    private void EndLevel()
    {
        EndGameStatus endGameStatus = new EndGameStatus();
        endGameStatus.win = true;
        endGameStatus.addedCoins = 0;
        Singleton<LevelManager>.Instance.EndLevel(endGameStatus);
    }

    private void StartBombDeactivation()
    {
        player.GetComponent<Hands>().SetNewWeapon(scissors);
        bomb.GetComponent<Bomb>().Activate();
        bomb.GetComponent<Bomb>().Level = this;
    }
}
