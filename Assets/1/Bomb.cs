using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bomb : MonoBehaviour
{
    [SerializeField]
    List<GameObject> wires;
    [HideInInspector]
    public Level Level;
    [SerializeField]
    Transform camPos;

    [SerializeField]
    private Light light;

    [SerializeField]
    GameObject cam;

    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    private bool isActivated = true;


    bool flag = false;

    public void Activate()
    {
        cam = Camera.main.gameObject;
        this.gameObject.SetActive(true);
        StartCoroutine(Lighthing());
        foreach (var item in wires)
        {
            item.SetActive(true);
        }
        flag = true;
    }

    IEnumerator Lighthing()
    {
        while (isActivated)
        {
            light.enabled = !light.enabled;
            yield return new WaitForSeconds(0.6f);

        }

    }


}
