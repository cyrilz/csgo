﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppLovinManager : MonoBehaviour
{
    public static AppLovinManager instance;
    public Interstitial Interstitial;
    public Rewarded Rewarded;
    public Banner Banner;

    [SerializeField] private bool EnableMediationDebugger;
    [SerializeField] private string appLovinKey = "6AQkyPv9b4u7yTtMH9PT40gXg00uJOTsmBOf7hDxa_-FnNZvt_qTLnJAiKeb5-2_T8GsI_dGQKKKrtwZTlCzAR";
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(gameObject);
    }

    private void OnEnable()
    {
        MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
        {
            Interstitial.InitializeInterstitialAds();
            Rewarded.InitializeRewardedAds();
            Banner.InitializeBannerAds();
            Banner.Show();
            
            if (EnableMediationDebugger)
                MaxSdk.ShowMediationDebugger();
        };    
        MaxSdk.SetSdkKey(appLovinKey);
        MaxSdk.InitializeSdk();
    }
}
