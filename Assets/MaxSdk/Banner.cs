﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class Banner : MonoBehaviour
{
    public readonly GlobalEvent OnShow = new GlobalEvent();
    
    [SerializeField] private string bannerAdUnitIdAndroid = "c9b4157b0a33a882";
    [SerializeField] private string bannerAdUnitIdIos = "d3320e29b29c2da3";
    
    private IDisposable timer;
    private string bannerAdUnitId;

    private void Awake()
    {
#if UNITY_IOS || UNITY_IPHONE
        bannerAdUnitId = bannerAdUnitIdIos;
#else        
        bannerAdUnitId = bannerAdUnitIdAndroid;
#endif
    }

    public void InitializeBannerAds()
    {
        // Banners are automatically sized to 320x50 on phones and 728x90 on tablets
        // You may use the utility method `MaxSdkUtils.isTablet()` to help with view sizing adjustments
        MaxSdk.CreateBanner(bannerAdUnitId, MaxSdkBase.BannerPosition.BottomCenter);

        // Set background or background color for banners to be fully functional
        MaxSdk.SetBannerBackgroundColor(bannerAdUnitId, Color.white);

        MaxSdkCallbacks.OnBannerAdLoadedEvent += place =>
        {
            timer = Observable.Timer(TimeSpan.FromSeconds(0.1f)).Subscribe(x =>
            {
                OnShow.Invoke();
            });
        };
    }

    public void Show()
    {
        MaxSdk.ShowBanner(bannerAdUnitId);
    }

    public void Hide()
    {
        MaxSdk.HideBanner(bannerAdUnitId);
    }
    
    private void OnDisable()
    {
        timer?.Dispose();
    }
}
