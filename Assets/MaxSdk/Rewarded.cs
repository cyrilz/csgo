﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

public class Rewarded : MonoBehaviour
{
    /// <summary>
    /// Вызывается когда кто-то пытается показать Rewarded рекламу
    /// </summary>
    public readonly GlobalEvent OnTryShow = new GlobalEvent();
    
    /// <summary>
    /// Вызывается в конце попытки показать рекламу, вне зависимости от результата показа (ошабка, успех или запрет)
    /// </summary>
    public readonly GlobalEvent OnTotalyClosed = new GlobalEvent();
    
    public readonly GlobalEvent<string> OnReady = new GlobalEvent<string>();
    public readonly  GlobalEvent<string> OnNotReady = new GlobalEvent<string>();
    public readonly GlobalEvent<string> OnDisplayed = new GlobalEvent<string>();
    public readonly GlobalEvent<string> OnFailedToDisplay = new GlobalEvent<string>();
    public readonly GlobalEvent<(string placement, string result)> OnDismissed = new GlobalEvent<(string, string)>(); // place, result

    [SerializeField] private string rewardedAdUnitIdAndroid = "c01f128df72749eb";
    [SerializeField] private string rewardedAdUnitIdIos = "428661a624b3e32b";
    
    private int retryAttempt;
    private string placement;
    private Action onWatched;
    private Action onCanceled;
    private Action onTotalyClosedLocaly;
    private bool isWatched;
    private IDisposable timerDisposable;
    private Button previousButton;
    private string rewardedAdUnitId;
    private bool skipInsterstitial;

    private void Awake()
    {
#if UNITY_IOS || UNITY_IPHONE
        rewardedAdUnitId = rewardedAdUnitIdIos;
#else        
        rewardedAdUnitId = rewardedAdUnitIdAndroid;
#endif
    }

    public void InitializeRewardedAds()
    {
        // Attach callback
        MaxSdkCallbacks.OnRewardedAdLoadedEvent += OnRewardedAdLoadedEvent;
        MaxSdkCallbacks.OnRewardedAdLoadFailedEvent += OnRewardedAdFailedEvent;
        MaxSdkCallbacks.OnRewardedAdFailedToDisplayEvent += OnRewardedAdFailedToDisplayEvent;
        MaxSdkCallbacks.OnRewardedAdDisplayedEvent += OnRewardedAdDisplayedEvent;
        MaxSdkCallbacks.OnRewardedAdHiddenEvent += OnRewardedAdDismissedEvent;
        MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;

        // Load the first RewardedAd
        LoadRewardedAd();

        OnTotalyClosed.Subscribe(() => skipInsterstitial = false);
    }
    private void OnDisable()
    {
        timerDisposable?.Dispose();
    }

    /// <summary>
    /// Инициирует открытие Rewarded рекламы
    /// </summary>
    /// <param name="place">Описание места открытия рекламы на английском, нижним регистром и в качестве пробелов '_'</param>
    /// <param name="onWatched">Вызывается когда игрок получает награду (успешно посмотрит рекламу)</param>
    /// <param name="onCanceled">Вызывается когда игрок закрывает рекламу, не досмотрев её</param>
    /// <param name="onTotalyClosed">Вызывается когда реклама закрылась в любом из случаев (ошибка, просмотр и отмена)</param>
    /// <param name="button">Кнопка, которая инициализирует открытие рекламы. Используется для того, чтобы блокировать множественное повторное нажатие в течении некоторго времени</param>
    /// <param name="skipInsterstitial">Пропустить ли интерстишиал рекламу в следующий раз, если игрок досмотрит этот ревардед</param>
    /// <returns></returns>
    public bool Show(string place, Action onWatched, Action onCanceled, Action onTotalyClosed, Button button, bool skipInsterstitial = false)
    {
        onTotalyClosedLocaly = onTotalyClosed;
        OnTryShow.Invoke();
        
        placement = place;
        this.onWatched = onWatched;
        this.onCanceled = onCanceled;
        isWatched = false;
        
        if (MaxSdk.IsRewardedAdReady(rewardedAdUnitId))
        {
            this.skipInsterstitial = skipInsterstitial;
            AppLovinManager.instance.Banner.Hide();
            
            if (button)
                button.interactable = false;
            
            OnReady.Invoke(placement);
            timerDisposable = Observable.TimerFrame(2).Subscribe(x =>
            {
                MaxSdk.ShowRewardedAd(rewardedAdUnitId);
            });

            previousButton = button;
            return true;
        }
        
        if (previousButton != button)
            OnNotReady.Invoke(placement);
        
        previousButton = button;
        OnTotalyClosed.Invoke();
        onTotalyClosedLocaly?.Invoke();
        return false;
    }

    private void LoadRewardedAd()
    {
        MaxSdk.LoadRewardedAd(rewardedAdUnitId);
    }

    private void OnRewardedAdLoadedEvent(string adUnitId)
    {
        retryAttempt = 0;
    }

    private void OnRewardedAdFailedEvent(string adUnitId, int errorCode)
    {
        retryAttempt++;
        double retryDelay = Math.Pow(2, Math.Min(6, retryAttempt));
        Observable.Timer(TimeSpan.FromSeconds(retryDelay)).Subscribe(x => { LoadRewardedAd(); });
    }

    private void OnRewardedAdFailedToDisplayEvent(string adUnitId, int errorCode)
    {
        OnFailedToDisplay.Invoke(placement);
        LoadRewardedAd();
        onCanceled?.Invoke();
        
        if (previousButton)
            previousButton.interactable = true;
        
        OnTotalyClosed.Invoke();
        onTotalyClosedLocaly?.Invoke();
    }

    private void OnRewardedAdDisplayedEvent(string adUnitId)
    {
        OnDisplayed.Invoke(placement);
    }

    private void OnRewardedAdDismissedEvent(string adUnitId)
    {
        LoadRewardedAd();
        OnDismissed.Invoke((placement, isWatched ? "watched" : "canceled"));
        
        if (!isWatched)
            onCanceled?.Invoke();

        if (previousButton)
            previousButton.interactable = true;
        
        AppLovinManager.instance.Banner.Show();
        isWatched = false;
        
        OnTotalyClosed.Invoke();
        onTotalyClosedLocaly?.Invoke();
    }

    private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward)
    {
        onWatched?.Invoke();
        isWatched = true;
        
        if (skipInsterstitial)
            AppLovinManager.instance.Interstitial.SkipNextAd();
    }
}