﻿using System;
using UniRx;
using UnityEngine;

public class Interstitial : MonoBehaviour
{
    /// <summary>
    /// Вызывается когда кто-то пытается показать Interstitial рекламу
    /// </summary>
    public readonly GlobalEvent OnTryShow = new GlobalEvent();
    
    /// <summary>
    /// Вызывается в конце попытки показать рекламу, вне зависимости от результата показа (ошабка, успех или запрет)
    /// </summary>
    public readonly GlobalEvent OnTotalyClosed = new GlobalEvent(); 
    
    public readonly GlobalEvent OnReady = new GlobalEvent();
    public readonly GlobalEvent OnNotReady = new GlobalEvent();
    public readonly GlobalEvent OnDisplayed = new GlobalEvent();
    public readonly GlobalEvent OnFailedToDisplay = new GlobalEvent();
    public readonly GlobalEvent OnDismissed = new GlobalEvent();

    [SerializeField] private string interstitialAdUnitIdAndroid = "4bda294e304dce28";
    [SerializeField] private string interstitialAdUnitIdIos = "79f337834d34bdef";
    [SerializeField] private int notShowDelay = 30;
    
    private int retryAttempt;
    private bool isWatchedRecently;
    private bool isSkiped;
    private IDisposable timerDisposable;
    private string interstitialAdUnitId;
    private Action onTotalyClosedLocaly;

    private void Awake()
    {
#if UNITY_IOS || UNITY_IPHONE
        interstitialAdUnitId = interstitialAdUnitIdIos;
#else        
        interstitialAdUnitId = interstitialAdUnitIdAndroid;
#endif
    }
    
    private void OnDisable()
    {
        timerDisposable?.Dispose();
    }

    public void InitializeInterstitialAds()
    {
        // Attach callback
        MaxSdkCallbacks.OnInterstitialLoadedEvent += OnInterstitialLoadedEvent;
        MaxSdkCallbacks.OnInterstitialLoadFailedEvent += OnInterstitialFailedEvent;
        MaxSdkCallbacks.OnInterstitialAdFailedToDisplayEvent += InterstitialFailedToDisplayEvent;
        MaxSdkCallbacks.OnInterstitialHiddenEvent += OnInterstitialDismissedEvent;
        MaxSdkCallbacks.OnInterstitialDisplayedEvent += OnInterstitialDisplayedEvent;  

        // Load the first interstitial
        LoadInterstitial();
    }

    /// <summary>
    /// Инициирует открытие Interstitial рекламы
    /// </summary>
    /// <param name="onTotalyClosed">Вызывается когда реклама закрылась в любом из случаев (ошибка, просмотр и отмена)</param>
    public void Show(Action onTotalyClosed)
    {
        onTotalyClosedLocaly = onTotalyClosed;
        OnTryShow.Invoke();

        if (isWatchedRecently || isSkiped)
        {
            OnTotalyClosed.Invoke();
            onTotalyClosedLocaly?.Invoke();
            isSkiped = false;
            return;
        }

        if (MaxSdk.IsInterstitialReady(interstitialAdUnitId))
        {
            AppLovinManager.instance.Banner.Hide();
            OnReady.Invoke();

            Observable.TimerFrame(2).Subscribe(x =>
            {
                MaxSdk.ShowInterstitial(interstitialAdUnitId);
            });
        }
        else
        {
            OnNotReady.Invoke();
            OnTotalyClosed.Invoke();
            onTotalyClosedLocaly?.Invoke();
        }
    }

    public void SkipNextAd()
    {
        isSkiped = true;
    }
    
    private void LoadInterstitial()
    {
        MaxSdk.LoadInterstitial(interstitialAdUnitId);
    }
    
    private void OnInterstitialLoadedEvent(string adUnitId)
    {
        retryAttempt = 0;
    }

    private void OnInterstitialFailedEvent(string adUnitId, int errorCode)
    {
        retryAttempt++;
        double retryDelay = Math.Pow(2, Math.Min(6, retryAttempt));
        Observable.Timer(TimeSpan.FromSeconds(retryDelay)).Subscribe(x => { LoadInterstitial(); });
    }

    private void InterstitialFailedToDisplayEvent(string adUnitId, int errorCode)
    {
        OnFailedToDisplay.Invoke();
        LoadInterstitial();
        OnTotalyClosed.Invoke();
        onTotalyClosedLocaly?.Invoke();
    }

    private void OnInterstitialDisplayedEvent(string adUnitId)
    {
        OnDisplayed.Invoke();
    }

    private void OnInterstitialDismissedEvent(string adUnitId)
    {
        OnDismissed.Invoke();
        AppLovinManager.instance.Banner.Show();
        isWatchedRecently = true;
        Observable.Timer(TimeSpan.FromSeconds(notShowDelay)).Subscribe(x => isWatchedRecently = false);
        LoadInterstitial();
        OnTotalyClosed.Invoke();
        onTotalyClosedLocaly?.Invoke();
    }
}