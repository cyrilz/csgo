using Menus;
using UnityEngine;
using UniRx;

public class LevelManager : Singleton<LevelManager>
{
    public PrefsValue<int> CurrentLevel = new PrefsValue<int>("CurrentLevel", 0);
    public PrefsValue<int> LevelCount = new PrefsValue<int>("LevelCount", 1);

    public int levelForTest = -1;

    public GameObject[] levels;

    private int timePlay = 0;
    private bool isActivate;

    private void Start()
    {
        isActivate = false;
        if (levelForTest > -1) {
            CurrentLevel.Value = levelForTest;
        }
        LoadLevel(CurrentLevel.Value);

        EventManager.OnStartGame.Subscribe(() =>
        {
            AppMetricaManager.instance.SendEvent("level_start", true, CurrentLevel.Value + 1, LevelCount.Value);
            LevelCount.Value++;

            Observable.Interval(1.sec()).TakeUntilDestroy(gameObject).Subscribe(x => timePlay++);
        });
    }

    public void LoadLevel(int _level) {
        //if (_level < levels.Length)
        //{
            levels[_level % levels.Length].SetActive(true);
        //}
        //else {
        //    Debug.Log("There are no more levels");
        //}
    }

    public void EndLevel(EndGameStatus status, bool skipReview = false)
    {
        if (isActivate && !skipReview) return;

        isActivate = true;
        levels[CurrentLevel.Value % levels.Length].SetActive(false);
        
        GUIManager.Close<GameplayMenu>();

        if (!skipReview && !status.win)
        {
            GUIManager.Open<ReviveMenu>();
            return;
        }

        EventManager.OnEndGame.Invoke(status);

        AppMetricaManager.instance.SendEvent("level_finish", true,
            CurrentLevel.Value + 1,
            status.win ? "win" : "lose",
            timePlay);

        if (status.win)
        {
            CurrentLevel.Value++;
        }
    }

    public GameObject GetCurrentLevel() {
        return levels[CurrentLevel.Value % levels.Length];
    }
}