using System;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

public class TerroristMoveController : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    [SerializeField]
    private NavMeshAgent navMeshAgent;

    private TerroristWeapon terroristWeapon;
    private bool isMoving = false;

    public bool IsMoving => isMoving;

    public void SetDestination(Vector3 position)
    {
        if (terroristWeapon)
            terroristWeapon.SetDefaultRotation();
        if (navMeshAgent.isActiveAndEnabled)
        {
            Observable.Timer(TimeSpan.FromSeconds(0.01f)).Subscribe(x =>
            {
                isMoving = true;
                navMeshAgent.destination = position;
                animator.SetFloat("CurrentSpeed", navMeshAgent.speed);
            });
        }
    }

    public void StopMoving()
    {
        isMoving = false;
        navMeshAgent.ResetPath();
        animator.SetFloat("CurrentSpeed", 0);
    }

    private void Start()
    {
        if (TryGetComponent<TerroristWeapon>(out TerroristWeapon terroristWeapon))
        {
            this.terroristWeapon = terroristWeapon;
        }

        animator.speed = UnityEngine.Random.Range(0.9f, 1.2f);
    }

    private void Update()
    {
        if (isMoving && navMeshAgent.remainingDistance < 0.1f)
        {
            StopMoving();
        }
    }
}
