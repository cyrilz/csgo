using UnityEngine;
using UnityEditor;
using System.Linq;

[CustomEditor(typeof(TerroristDeath))]
public class TerroristDeathEditor : Editor
{
    TerroristDeath terroristDeath;

    Collider collider;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        terroristDeath = (TerroristDeath)target;

        collider = terroristDeath.GetComponentInChildren<Collider>();

        if (GUILayout.Button("Set Up All Rigidbodies"))
        {
            terroristDeath.rigidbodies = terroristDeath.transform.GetComponentsInChildren<Rigidbody>();

            foreach (Rigidbody rigidbody in terroristDeath.rigidbodies) {
                rigidbody.useGravity = false;
                rigidbody.isKinematic = true;
            }

            Debug.Log("Rigidbodies found: " + terroristDeath.rigidbodies.Length);
        }
    }
}
