using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerroristLogic : MonoBehaviour
{
    [SerializeField] private TerroristMoveController moveController;
    [SerializeField] private Transform target;
    [SerializeField] private float terroristSpeedRotation;
    [SerializeField, Range(0.0f, 1.0f)] private float idleAnimationSpeed = 1.0f;
    [SerializeField] private Animator terroristAnimator;

    private const int IDLE_LAYER = 0;
    private const string idleAnimaton = "demo_combat_idle";

    public float TerroristSpeedRotation
    {
        get => terroristSpeedRotation;
        set => terroristSpeedRotation = value;
    }

    private void Start()
    {
        terroristAnimator.Play(idleAnimaton, IDLE_LAYER, idleAnimationSpeed);
    }

    public void Activate()
    {
        moveController.SetDestination(target.position);
    }

    public void TurnOnPlayer()
    {
        Vector3 terroristDirection = PlayerController.Instance.transform.position - transform.position;
        Quaternion terroristRotation = Quaternion.LookRotation(terroristDirection);

        transform.rotation = Quaternion.Lerp(transform.rotation, terroristRotation, Time.deltaTime * terroristSpeedRotation);
    }

    public void TurnOnRotation(Vector3 terroristRotation)
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(terroristRotation), Time.deltaTime * terroristSpeedRotation*10f);
    }
}
