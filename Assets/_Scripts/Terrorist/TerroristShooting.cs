using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class TerroristShooting : MonoBehaviour
{
    [SerializeField] private Transform muzzle;

    [SerializeField] private Animator animator;

    [SerializeField] private GameObject bulletPrefab;

    [SerializeField] private GameObject questionSign; //?
    [SerializeField] private GameObject exclamationSign; //!
    [SerializeField] private GameObject questionUI;
    [SerializeField] private GameObject layoutQuestion;

    [SerializeField] private bool shootAtStart = true;
    [SerializeField] private float _time = 3.0f;
    [SerializeField] private float _repeatTime = 3.0f;

    private const string SHOT_TRIGGER = "Shot";

    private bool suspicion = false;
    private bool playerDetected = false;

    private TerroristWeapon terroristWeapon;

    public bool Suspicion
    {
        get { return suspicion; }
        set
        {
            if (gameObject.activeInHierarchy)
            {
                suspicion = value;
                questionSign.SetActive(value);
            }
        }
    }

    public bool PlayerDetected
    {
        get { return playerDetected; }
        set
        {
            if (gameObject.activeInHierarchy)
            {
                playerDetected = value;
                questionSign.SetActive(!value);
                exclamationSign.SetActive(value);
            }
        }
    }

    public bool ShootAtStart
    {
        set => shootAtStart = value;
    }

    public float RepeatTime
    {
        set => _repeatTime = value;
    }

    public float FirstDelay
    {
        set => _time = value;
    }

    public void AddQuestionMark()
    {
        GameObject mark = Instantiate(questionUI);
        mark.transform.SetParent(layoutQuestion.transform,false);
    }

    private void Start()
    {
        InitBulletPosition();

        if (shootAtStart)
        {
            Observable.Interval(TimeSpan.FromSeconds(Time.timeScale == 0.3f ? 1f : 3f)).RepeatUntilDisable(gameObject)
                .Subscribe(x => { AttackPlayer(); });
        }
    }

    public void InitBulletPosition()
    {
        if (TryGetComponent<TerroristWeapon>(out TerroristWeapon terroristWeapon))
        {
            this.terroristWeapon = terroristWeapon;
            muzzle = terroristWeapon.SpawnBulletPos;
        }
    }

    public void AttackPlayer()
    {
        animator.SetTrigger(SHOT_TRIGGER);
        transform.LookAt(PlayerController.Instance.transform.position);
        if (terroristWeapon)
            terroristWeapon.SetShootRotation();
        Observable.Timer(TimeSpan.FromSeconds(0.3f)).TakeUntilDisable(gameObject)
            .Subscribe(_ =>
            {
                GameObject newBullet = Instantiate(bulletPrefab, muzzle.position, Quaternion.identity);

                newBullet.transform.LookAt(PlayerController.Instance.transform.position + new Vector3(0f, 1f, 0f));
                newBullet.GetComponent<MovingForward>().TargetPos = PlayerController.Instance.transform.position + new Vector3(0f, 1f, 0f);
                newBullet.GetComponent<MovingForward>().MoveToTarget = true;
                newBullet.GetComponent<MovingForward>().IsActivate = true;
                
                if(newBullet.GetComponent<CollisionDetection>())
                    newBullet.GetComponent<CollisionDetection>().SetOwner(GetComponent<TerroristDeath>());
            });
    }

    public void ShootOnTarget(Transform target, bool moveToTaregt = false, float offsetShotY = 0)
    {
        animator.SetTrigger(SHOT_TRIGGER);
        // transform.LookAt(target.position);

        IDisposable rotateDis;

        rotateDis = Observable.EveryUpdate().TakeUntilDisable(gameObject).Subscribe(x => 
        {
            Vector3 direction = target.transform.position - transform.position;
            Quaternion rotate = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotate, 5f * Time.deltaTime);
        });

        if (terroristWeapon)
            terroristWeapon.SetShootRotation();
        Observable.Timer(TimeSpan.FromSeconds(0.3f)).TakeUntilDisable(gameObject)
            .Subscribe(_ =>
            {
                rotateDis.Dispose();
                GameObject bullet = Instantiate(bulletPrefab, muzzle.position, Quaternion.identity);
                bullet.transform.LookAt(target.position + Vector3.up * offsetShotY);

                if (moveToTaregt)
                {
                    bullet.GetComponent<MovingForward>().TargetPos = target.position;
                    bullet.GetComponent<MovingForward>().MoveToTarget = true;
                }

                bullet.GetComponent<MovingForward>().IsActivate = true;
            });
    }
}
