using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TerroristDeath : HealthPoints
{
    [SerializeField]
    private Animator animator;
    [SerializeField] private bool dropWeaponWithDeath;
    [SerializeField] private GameObject weaponToDrop;

    [SerializeField] private int countHitsForDeath = 1;

    [HideInInspector] public Rigidbody[] rigidbodies;

    private const int ZERO = 0;

    private int countWound = 0;
    private Collider hitPosition = null;

    public Collider HitPosition { get => hitPosition; private set => hitPosition = value; }

    public float Health { get => healthPoints; set => healthPoints = value; }

    public override void GetHit(Collider collider, Vector3 force, float time = 0, bool showDeathMarker = true)
    {
        HitPosition = collider;
        countWound++;

        if (countWound == countHitsForDeath)
        {
            if(showDeathMarker) HitMarker.Instance.OnTerroristDeath.Invoke(this);

            animator.enabled = false;

            foreach (Rigidbody rigidbody in rigidbodies)
            {
                rigidbody.velocity = Vector3.zero;
                rigidbody.useGravity = true;
                rigidbody.isKinematic = false;
            }

            collider.GetComponent<Rigidbody>()?.AddForce(force);

            animator.transform.parent = null;

            if (dropWeaponWithDeath && weaponToDrop != null)
                DropWeapon();

            Destroy(gameObject);
        }
        else if (countWound < countHitsForDeath)
        {
            HitMarker.Instance.OnHit.Invoke(this);
        }
    }

    public override void GetHit(float damage, Collider collider, Vector3 force, bool showDeathMarker = true)
    {
        healthPoints -= damage;

        if (healthPoints <= 0) GetHit(collider, force, ZERO ,showDeathMarker);
    }

    private void DropWeapon()
    {
        var weapon = Instantiate(weaponToDrop, transform.position + new Vector3(0f, 1.8f, 0f), transform.rotation);

        weapon.GetComponent<Weapon>().DeactivateHands();
        weapon.GetComponent<BoxCollider>().enabled = true;
        weapon.GetComponent<Rigidbody>().useGravity = true;
        weapon.GetComponent<Rigidbody>().AddForce(-50f, 0f, 0f);
    }
}
