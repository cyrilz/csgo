using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerroristWeapon : MonoBehaviour
{
    [SerializeField] private GameObject weaponPrefab;
    [SerializeField] private Transform weaponPivot;
    [SerializeField] private Vector3 weaponRotation = new Vector3(-54.32f, 18.81f, 74.4f);
    [SerializeField] private Vector3 weaponShootRotation = new Vector3(-64.843f, 63.234f, -15.467f);

    [HideInInspector] public Transform SpawnBulletPos;

    private GameObject weapon;
    private void Awake()
    {
        if (weaponPrefab)
        {
            weapon = Instantiate(weaponPrefab, weaponPivot.position, weaponPivot.rotation);
            weapon.transform.SetParent(weaponPivot);
            weapon.transform.localRotation = Quaternion.Euler(weaponRotation);
            SpawnBulletPos = weapon.GetComponent<TerroristWeaponMuzzle>().SpawnBulletPos;
        }
    }

    public void SetShootRotation()
    {
        if(weapon)
            weapon.transform.localRotation = Quaternion.Euler(weaponShootRotation);
    }

    public void SetDefaultRotation()
    {
        if (weapon)
            weapon.transform.localRotation = Quaternion.Euler(weaponRotation);
    }
}
