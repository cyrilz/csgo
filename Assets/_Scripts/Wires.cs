using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Wires : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] Vector3 offset;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (Kusachki.Instance.Delay > 0)
            return;

        if (!Kusachki.Instance.enabled)
            return;

        //Debug.Log("Works");
        Kusachki.Instance.TargetCablePos = transform.position + offset;
        var t = transform.GetChildes();
        foreach (var item in t)
        {
            item.parent = this.transform.parent;
            item.gameObject.SetActive(true);
        }
        Destroy(gameObject);
    }
}
