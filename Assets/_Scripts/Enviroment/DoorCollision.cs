using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCollision : MonoBehaviour
{
    [SerializeField] private GameObject vfxCollision;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            Instantiate(vfxCollision, transform.position, transform.rotation);
        }
    }
}
