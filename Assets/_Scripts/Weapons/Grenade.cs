using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class Grenade : Weapon
{
    public float throwingForce;
    public bool isCheckPulled = false;
    public GameObject LastTargetPoint;

    [SerializeField] Animator animator_hands;
    [SerializeField] private GameObject throwPivot;
    [SerializeField] private DrawTrajectory trajectory;
    [SerializeField] private LineRenderer lineRenderer;

    private float cooldown = 1f;
    private float minHoldTime = 0.45f;
    private float currnetHoldTime;
    private bool isThrowed = false;

    private Quaternion endRotation = new Quaternion(0f, -0.5f, 0.2f, 0.8f);

    private void Start()
    {
        animator_hands.SetTrigger("taking");
    }

    private void Update()
    {
        if (isThrowed) return;

        Vector2 clickPosition = Vector2.zero;

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            clickPosition = touch.position;
        }

        if (Input.GetAxis("Fire1") > 0)
        {
            clickPosition = Input.mousePosition;
        }

        if (clickPosition != Vector2.zero)
        {
            currnetHoldTime += Time.deltaTime;

            Vector3 target;

            Ray ray = Camera.main.ScreenPointToRay(clickPosition);

            target = ray.direction * 1000 + Camera.main.transform.position;

            if (currnetHoldTime > minHoldTime)
            {
                isCheckPulled = true;
                throwPivot.transform.LookAt(target);
            }
        }
        else
        {
            if (currnetHoldTime > minHoldTime && !isThrowed)
            {
                Attack(Vector3.zero);
                isThrowed = true;

                Observable.EveryUpdate().TakeUntilDisable(gameObject)   
                    .SkipUntil(Observable.Timer(TimeSpan.FromSeconds(0.5)))
                    .Subscribe(x =>
                    {
                        throwPivot.transform.rotation = Quaternion.Lerp(transform.rotation, endRotation, Time.deltaTime * 4);
                    });

                trajectory.enabled = false;
                lineRenderer.enabled = false; // hide line renderer
                LastTargetPoint.SetActive(false);
            }
            isCheckPulled = false;
        }
    }

    public override void Attack(Vector3 target, bool followingBullet = false)
    {
        animator_hands?.Play("Grenade");       
    }    
}