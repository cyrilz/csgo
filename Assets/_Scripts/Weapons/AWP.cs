using UniRx;
using UnityEngine;

public class AWP : Weapon
{
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private GameObject bulletFlashVFX;
    [SerializeField] private Animator AK_anim;
    [SerializeField] private Animator Hands_anim;
    [SerializeField] private Transform muzzle;
    [SerializeField] private float delayShot = 0;

    private const bool ACTIVATED = true, DEACTIVATED = false;

    private Vector3 target;
    private Camera cam;

    private void Start()
    {
        cam = Camera.main;

        if (gameObject.name == "Sniper(Clone)")
        {
            AK_anim.SetTrigger("Taking");
            Hands_anim.SetTrigger("Taking");
        }

        StopFiring();
    }

    public override void Attack(Vector3 target, bool followingBullet = false)
    {
        if (Time.time - timeOfLastShot > timeBetweenShots + delayShot)
        {
            timeOfLastShot = Time.time;
            Observable.Timer(delayShot.sec())
                .Subscribe(x =>
                {
                    StartFiring();

                    GameObject newBullet = Instantiate(bulletPrefab, muzzle.position, Quaternion.identity);
                    GameObject vfx = Instantiate(bulletFlashVFX, muzzle.position + Vector3.up * 0.04f, transform.rotation, transform);

                    newBullet.transform.LookAt(target);

                    if (followingBullet)
                    {
                        newBullet.GetComponent<MovingForward>().TargetPos = target;
                        newBullet.GetComponent<MovingForward>().MoveToTarget = true;
                    }
                    //newBullet.GetComponent<MovingForward>().IsActivate = true;

                    //transform.LookAt(target);
                    this.target = target;

                    Destroy(vfx, 1f);
                });
        }
    }

    private void StopFiring()
    {

    }

    private void StartFiring()
    {
        if (AK_anim)
        {
            AK_anim.Play("Firing");
        }
        if (Hands_anim)
        {
            Hands_anim.Play("Firing");
        }
    }
}
