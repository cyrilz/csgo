using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeAlarm : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        var t = FindObjectsOfType<TerroristShooting>();
        foreach (var item in t)
        {
            item.AttackPlayer();
        }
    }
}
