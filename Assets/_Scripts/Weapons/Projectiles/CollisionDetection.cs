using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetection : MonoBehaviour
{
    [SerializeField] private float bulletForce;
    [SerializeField] private bool terrorBullet;

    private HealthPoints owner;

    public void SetOwner(HealthPoints owner)
    {
        this.owner = owner;
    }

    private void OnEnable()
    {
        StartCoroutine(DestroyInTime());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (terrorBullet)
        {
            /*if (gameObject.name == "Bullet_Terrorist(Clone)") Debug.Log(Vector3.Distance(transform.position, PlayerController.Instance.transform.position));
              if (other.name == "Bone")
              {
                  Destroy(gameObject);
                  return;
              }
              else if (gameObject.name == "Bullet(Clone)" && !other.CompareTag("Player"))
              {
                  DoDamage(other);
              }
              else if (gameObject.name == "Bullet_Terrorist(Clone)"
                  && Vector3.Distance(transform.position, PlayerController.Instance.transform.position) <= 1.15f)
              {
                  PlayerController.Instance.GetComponent<HealthPoints>()?.GetHit(null, Vector3.zero);
              }*/

            if (other.CompareTag("Player"))
                DoDamage(other, 0.0f);
            else if(Vector3.Distance(transform.position, PlayerController.Instance.transform.position) <= 1.15f)
            {
                if (owner != null)
                    PlayerController.Instance.GetComponent<HealthPoints>()?.GetHit(null, Vector3.zero);
                Destroy(gameObject);
            }
        }
        else
        {
            if (other.CompareTag("Enemy"))
            {
                DoDamage(other);
            }
        }
    }

    private void DoDamage(Collider target, float time = 0)
    {
        //need to determine when the enemy's bullet hits the player - rigidbody isn't suitable for this
        //gameover doesn't work at 6 lvl

        if (terrorBullet && owner == null)
            return;

        target.GetComponentInParent<HealthPoints>()?.GetHit(target, transform.forward * bulletForce, time);
        Destroy(gameObject);

    }

    IEnumerator DestroyInTime()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }
}
