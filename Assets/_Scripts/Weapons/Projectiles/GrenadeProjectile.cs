using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

public class GrenadeProjectile : MonoBehaviour
{
    [SerializeField] private float explosionRadius;
    [SerializeField] private float explosionForce;
    [SerializeField] private GameObject explosionVFX;

    private Level_6_Script_New levelScript;

    private void Start()
    {
        levelScript = GameObject.Find("Level_6_WithGrenade").GetComponent<Level_6_Script_New>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        Invoke(nameof(Explosion), 0.5f);
    }

    private void Explosion()
    {
        GameObject vfx = Instantiate(explosionVFX, transform.position, Quaternion.identity);
        Destroy(vfx, 3f);

        RaycastHit[] hits = Physics.SphereCastAll(transform.position, explosionRadius, transform.forward);
        if (hits.Length > 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                hits[i].collider.GetComponentInParent<HealthPoints>()?.GetHit(hits[i].collider,
                    (hits[i].collider.transform.position - transform.position).normalized * explosionForce, 0, false);
            }
        }

        levelScript.TriggerTerroristAggressive();

        Destroy(gameObject);
    }
}
