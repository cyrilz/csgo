using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

public class MovingForward : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] public bool IsActivate;

    [HideInInspector] public Vector3 TargetPos = Vector3.zero;
    [HideInInspector] public bool MoveToTarget;

    private bool isDestroy = false;

    public float Speed { 
        get { return speed; }
        set { speed = value; } }

    private void FixedUpdate()
    {
        if (!IsActivate) return;
        if (MoveToTarget && TargetPos != Vector3.zero)
        {
            transform.position = Vector3.Lerp(transform.position, TargetPos, speed * Time.fixedDeltaTime);
            if (Vector3.Distance(transform.position, TargetPos) <= 0.1f && !isDestroy)
            {
                isDestroy = true;
                Observable.Timer(0.2f.sec()).TakeUntilDisable(gameObject).Subscribe(_ => Destroy(gameObject));
            }
        }
        else
        {
            transform.position += transform.forward * speed * Time.fixedDeltaTime;
        }
    }
}
