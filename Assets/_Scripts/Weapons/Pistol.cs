using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class Pistol : Weapon
{
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private GameObject bulletFlashVFX;

    [SerializeField] private Transform muzzle;
    [SerializeField] private float delayShot = 0;

    public override void Attack(Vector3 target, bool followingBullet = false)
    {
        if (Time.time - timeOfLastShot > timeBetweenShots + delayShot)
        {
            timeOfLastShot = Time.time;
            Observable.Timer(delayShot.sec())
                .Subscribe(x =>
                {
                    GameObject newBullet = Instantiate(bulletPrefab, muzzle);
                    GameObject vfx = Instantiate(bulletFlashVFX, muzzle.position + Vector3.up * 0.07f, transform.rotation, transform);

                    newBullet.transform.SetParent(null);

                    if (followingBullet)
                    {
                        newBullet.GetComponent<MovingForward>().TargetPos = target;
                        newBullet.GetComponent<MovingForward>().MoveToTarget = true;
                    }

                    Destroy(vfx, 1f);
                });
        }
    }
}
