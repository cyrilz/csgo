using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class Kusachki : Singleton<Kusachki>
{
    [SerializeField] private GameObject topHandle;
    [SerializeField] private GameObject downHandle;

    [SerializeField] private float delayWire;

    [SerializeField] private Transform kusachkiStartTransform;

    private Vector3 endPos = new Vector3(0.131f, 1.062f, -0.734f);
    private Vector3 startPos = new Vector3(0.184f, 1.573f, -1.076f);

    private Vector3 targetCablePos = Vector3.zero;
    private bool canCut = false;

    [HideInInspector] public float Delay;

    public Vector3 TargetCablePos { set { targetCablePos = value; } }

    private void Start()
    {
        transform.position = kusachkiStartTransform.position;
    }

    private void Update()
    {
        Delay -= Time.deltaTime;

        if (Input.GetMouseButtonDown(0) && Delay <= 0)
        {
            Delay = delayWire;
            topHandle.transform.localEulerAngles = new Vector3(topHandle.transform.localEulerAngles.x, topHandle.transform.localEulerAngles.y, 70f);
            downHandle.transform.localEulerAngles = new Vector3(downHandle.transform.localEulerAngles.x, downHandle.transform.localEulerAngles.y, 70f);
            MoveToCable();
        }
    }

    private void MoveToEndPos()
    {
        Observable.EveryFixedUpdate().TakeWhile(x =>
        {
            if (Vector3.Distance(transform.localPosition, endPos) >= 0.01f) return true;
            canCut = true;
            return false;
        }).Subscribe(y =>
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, endPos, Time.fixedDeltaTime / 3);

            topHandle.transform.localEulerAngles = Vector3.Lerp(topHandle.transform.localEulerAngles
                , new Vector3(topHandle.transform.localEulerAngles.x, topHandle.transform.localEulerAngles.y, 70f), Time.fixedDeltaTime * 1.3f);

            downHandle.transform.localEulerAngles = Vector3.Lerp(downHandle.transform.localEulerAngles, 
                new Vector3(downHandle.transform.localEulerAngles.x, downHandle.transform.localEulerAngles.y, 70f), Time.fixedDeltaTime * 1.3f);
        });
    }

    private void MoveToCable()
    {
        if(targetCablePos != Vector3.zero)
        {
            Observable.EveryFixedUpdate().TakeWhile(x =>
            {
                if (Vector3.Distance(transform.position, targetCablePos) >= 0.005f) return true;
                CutAnimation();
                targetCablePos = Vector3.zero;
                return false;
            }).Subscribe(y =>
            {
                transform.position = Vector3.MoveTowards(transform.position, targetCablePos, Time.fixedDeltaTime * 2);
            });
        }
    }

    private void CutAnimation()
    {
        bool isAnimationEnd = false;
        var topEulerAngles = topHandle.transform.localEulerAngles;
        var downEulerAngles = downHandle.transform.localEulerAngles;
        topEulerAngles.z = 95f;
        downEulerAngles.z = 95f;

        Observable.EveryFixedUpdate().TakeWhile(x =>
        {
            if (isAnimationEnd) return false;
            return true;
        }).Subscribe(y =>
        {
            topHandle.transform.localEulerAngles = Vector3.Lerp(topHandle.transform.localEulerAngles, topEulerAngles, Time.fixedDeltaTime * 10);
            downHandle.transform.localEulerAngles = Vector3.Lerp(downHandle.transform.localEulerAngles, downEulerAngles, Time.fixedDeltaTime * 10);

            if (topHandle.transform.localEulerAngles.z - 89f >= 0)
            {
                topEulerAngles.z = 65f;
                downEulerAngles.z = 65f;
            }
            else if (topHandle.transform.localEulerAngles.z - 71f <= 0)
            {
                isAnimationEnd = true;
            }
        });
    }
}
