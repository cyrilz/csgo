using DG.Tweening;
using UniRx;
using UnityEngine;

public class AK47 : Weapon
{
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private GameObject bulletFlashVFX;
    [SerializeField] private Animator AK_anim;
    [SerializeField] private Animator Hands_anim;
    [SerializeField] private Transform muzzle;
    [SerializeField] private float delayShot = 0;
    [SerializeField] private bool enableCrossHair = true;
    [SerializeField] private LayerMask hitLayers;

    [HideInInspector] public float bulletSpeedMult = 1;

    private Vector3 target;
    private Camera cam;


    private void Start()
    {
        cam = Camera.main;

        //AimSights.Instance.gameObject.SetActive(enableCrossHair);

        /*
        if (gameObject.name == "Sniper(Clone)")
        {
            Debug.Log("123");
            AK_anim.SetTrigger("Taking");
            Hands_anim.SetTrigger("Taking");
        }
        */

        Hands_anim.SetTrigger("Taking");
        AK_anim.SetTrigger("Taking");
        StopFiring();
    }

    public override void Attack(Vector3 target, bool followingBullet = false)
    {
        if (Time.time - timeOfLastShot > timeBetweenShots + delayShot)
        {
            timeOfLastShot = Time.time;
            Observable.Timer(delayShot.sec())
                .Subscribe(x =>
                {
                    StartFiring();
                    gameObject.transform.DOScale(transform.localScale, .3f)
                        .OnComplete(() =>
                        {
                            StopFiring();
                        });

                    GameObject newBullet = Instantiate(bulletPrefab, muzzle);
                    GameObject vfx = Instantiate(bulletFlashVFX, muzzle.position + Vector3.up * 0.04f, transform.rotation, transform);

                    newBullet.transform.Rotate(0, 180, 0);
                    newBullet.transform.SetParent(null);
                    newBullet.GetComponent<MovingForward>().Speed *= bulletSpeedMult; 
                    if (followingBullet)
                    {
                        // newBullet.GetComponent<MovingForward>().TargetPos = target;
                        newBullet.GetComponent<MovingForward>().MoveToTarget = true;
                    }
                    //newBullet.GetComponent<MovingForward>().IsActivate = true;

                    //transform.LookAt(target);
                    this.target = target;

                    Destroy(vfx, 1f);
                });
        }
    }

    private void Update()
    {
        if (enableCrossHair)
        {
            UpdateCrosshairPosition();
        }

        //transform.LookAt(target);
    }

    private void StopFiring()
    {
        if (AK_anim)
        {
            AK_anim.Play("AK_Idle");
        }
        if (Hands_anim)
        {
            Hands_anim.Play("AK_Idle");
        }
    }

    private void StartFiring()
    {
        if (AK_anim)
        {
            AK_anim.Play("Firing");
        }
        if (Hands_anim)
        {
            Hands_anim.Play("Firing");
        }
    }

    private void UpdateCrosshairPosition()
    {
        Ray ray = new Ray(muzzle.position, -muzzle.forward);

        Vector2 uiPosition = Physics.Raycast(ray, out RaycastHit hit, 1000, hitLayers)
            ? (Vector2)cam.WorldToScreenPoint(hit.point)
            : (Vector2)cam.WorldToScreenPoint(ray.direction * 1000);

        AimSights.Instance.UpdatePosition(uiPosition);
    }

    public void StartWalkingAnimation()
	{
        AK_anim.SetTrigger("Moved");
        Hands_anim.SetTrigger("Moved");
	}

    public void StartIdleAnimation()
	{
        AK_anim.SetTrigger("StopMoved");
        Hands_anim.SetTrigger("StopMoved");
    }
}
