using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    [SerializeField] private Vector3 closePosition;
    [SerializeField] private Vector3 closeRotation;
    [SerializeField] private Vector3 openPosition;
    [SerializeField] private Vector3 openRotation;

    [SerializeField] private float changeStateSpeed = 3f;

    [SerializeField] private Collider collider;

    [HideInInspector] private bool isClosing = false;
    [HideInInspector] private bool isOpening = false;

    public bool IsClosing { get => isClosing; set { isClosing = value; collider.enabled = true; } }
    public bool IsOpening { get => isOpening; set { isOpening = value; collider.enabled = false; } }

    //private Vector3 positionBorder = new Vector3(0.1f, 0.1f, 0.1f);

    private void Start()
    {
       // transform.localPosition = closePosition;
    }

    private void FixedUpdate()
    {
        //if (IsClosing)
        //{
        //    if (IsOpening) IsOpening = false;

        //    if(!MoveShield(closePosition, closeRotation))
        //    {
        //        IsClosing = false;
        //    }
        //}
        
        //if (IsOpening)
        //{
        //    if (IsClosing) IsClosing = false;

        //    if (!MoveShield(openPosition, openRotation))
        //    {
        //        IsOpening = false;
        //    }
        //}
    }

    private bool MoveShield(Vector3 targetPosition, Vector3 targetEulerRotation)
    {
        if (Vector3.Distance(transform.localPosition, targetPosition) > 0.02f)
        {
            //transform.eulerAngles = Vector3.Lerp(transform.localRotation.eulerAngles, targetEulerRotation, changeStateSpeed * Time.fixedDeltaTime);
            transform.eulerAngles = targetEulerRotation;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPosition, changeStateSpeed * Time.fixedDeltaTime);
            return true;
        }

        return false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Bullet")) 
            Destroy(other.gameObject);
    }
}
