using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class Knife : Weapon
{
    [SerializeField]
    Animator animator;
    [SerializeField]
    Animator animator_hands;
    private int damage = 0;
    private const float timeToHit = 0.2f;

    public int Damage { get { return damage; } set { damage = value; } }
    public Animator KnifeAnimator => animator;

    public bool playTaking = false;

    private void Start()
    {
        animator.enabled = true;
        animator_hands.enabled = true;

        animator.SetTrigger("Taking");
        animator_hands.SetTrigger("Taking");
    }

    public override void Attack(Vector3 target, bool followingBullet = false)
    {
        if (TimeToShoot())
        {
            timeOfLastShot = Time.time;
            animator?.Play("Hit");
            animator_hands?.Play("Hit");
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit raycastHit;

            if(Physics.SphereCast(ray.origin, 1, ray.direction, out raycastHit, 1)){
                if (raycastHit.rigidbody != null)
                { //work only with assigned damage like hands.currentWeaponObj.GetComponent<Knife>().Damage = damage from other script
                    if (damage != 0)
                    {
                        Observable.Timer(TimeSpan.FromSeconds(timeToHit)).TakeUntilDisable(gameObject)
                            .Subscribe(x => 
                            {
                                raycastHit.transform.GetComponentInParent<HealthPoints>()?
                                .GetHit(damage, raycastHit.collider, Vector3.zero, false);
                            });
                    }
                }
            }
        }
    }
    
    public bool TimeToShoot()
    {
        return Time.time - timeOfLastShot > timeBetweenShots;
    }

    public void StartWalkingAnimation()
    {
        IDisposable walking = null;

        walking = Observable.EveryUpdate().TakeUntilDisable(gameObject)
            .SkipWhile(w =>
            {
                if (animator_hands.GetCurrentAnimatorClipInfoCount(0) > 1) return true;
                return false;
            }).Subscribe(x=> 
            {
                animator_hands.SetBool("Walking", true);
                walking?.Dispose();
            });
    }

    public void StartIdleAnimation()
    {
        animator_hands.SetBool("Walking", false);
    }
}
