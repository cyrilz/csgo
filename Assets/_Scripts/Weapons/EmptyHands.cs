using UnityEngine;

[DisallowMultipleComponent]
public class EmptyHands : Weapon
{
    [SerializeField] private Animator emptyHands = null;

    private const string HAND_TO_AK = "Hand_to_AK";

    public void PlayAnimationHandToAK()
    {
        emptyHands.Play(HAND_TO_AK);
    }

    public override void Attack(Vector3 target, bool followingBullet = false) { }
}
