using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    [SerializeField] protected float timeBetweenShots;
    [SerializeField] private GameObject hands;

    protected float timeOfLastShot = 0;

    public abstract void Attack(Vector3 target, bool followingBullet = false);

    public void DeactivateHands()
    {
        hands.SetActive(false);
    }
}
