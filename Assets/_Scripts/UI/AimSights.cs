using UnityEngine;

[DisallowMultipleComponent]
public class AimSights : Singleton<AimSights>
{
    [SerializeField] private GameObject _crosshair;
    private RectTransform rectTransform = null;

    public GlobalEvent<bool> CrosshairStateChangeEvent = new GlobalEvent<bool>();
    public bool AimToCenter;

    private Vector2 centerScreen;

    private void Start()
    {
        AimToCenter = false;
        centerScreen = new Vector2(Screen.width / 2, Screen.height / 2);
        rectTransform = GetComponent<RectTransform>();
        CrosshairStateChangeEvent.Subscribe(CrosshairStateChange);
    }

    public void UpdatePosition(Vector2 positionOnUi)
    {
        if(AimToCenter)
            rectTransform.position = centerScreen;
        else
            rectTransform.position = positionOnUi;
    }

    private void CrosshairStateChange(bool state) => _crosshair.SetActive(state);
}
