using System;
using Menus;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

public class ReviveMenu : UIMenu
{
    [SerializeField] private ReviveTimer reviveTimer;
    [SerializeField] private Button reviveButton;
    [SerializeField] private GameObject noThanksButton = null;
    [SerializeField] private float showUpTime = 0.5f;

    private const bool ACTIVATED = true;

    private void OnEnable()
    {
        reviveTimer.RestartTimer();
    }

    private void Start()
    {
        reviveTimer.OnTimerEnded += () => Lose();
        reviveTimer.OnTimerRestart += () => Restart();
    }

    private void Restart()
    {
        Observable.Timer(TimeSpan.FromSeconds(showUpTime)).TakeUntilDisable(this)
            .Subscribe(h =>
            {
                noThanksButton.SetActive(ACTIVATED);
            });
    }

    public void Lose()
    {
        LevelManager.Instance.EndLevel(new EndGameStatus { win = false },true);
        GUIManager.Close<ReviveMenu>();
    }

    public void ShowReward()
    {
        AppLovinManager.instance.Rewarded.Show("skip", () => { OnWatched(); }, () => { OnCanceled(); }, () => { OnTotalyClosed(); }, reviveButton, true);
        AppLovinManager.instance.Rewarded.OnReady.Subscribe((stopTimer) => reviveTimer.StopTimer());
    }

    private void OnWatched()
    {
        LevelManager.Instance.EndLevel(new EndGameStatus { win = true },true);
        GUIManager.Close<ReviveMenu>();
    }

    private void OnCanceled()
    {
        reviveTimer.ResumeTimer();
    }
    private void OnTotalyClosed()
    {
        reviveTimer.ResumeTimer();
    }

}