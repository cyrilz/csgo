using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleEffect : MonoBehaviour
{
    [SerializeField] private Transform _gameObjectScale;
    [SerializeField] private float _speedDecreaseScale;
	[SerializeField] private float _speedIncreaseScale;
	private float _currentSpeed;
	private Vector3 _currentScale;

	private void Start()
	{
		_gameObjectScale = GetComponent<Transform>();
		_currentScale = _gameObjectScale.localScale;
		_currentSpeed = _speedDecreaseScale;
	}

	private void Update()
	{
		Scaler();
		if (_currentScale.x < 1050)
			_currentSpeed = _speedIncreaseScale;
		else if (_currentScale.x > 1300)
			_currentSpeed = _speedDecreaseScale;
	}

	private void Scaler()
	{
		_currentScale = new Vector3
			(_currentScale.x - _currentSpeed,
			_currentScale.y - _currentSpeed, 
			_currentScale.z - _currentSpeed);

		_gameObjectScale.localScale = _currentScale;
	}
}
