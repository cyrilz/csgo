using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageText : MonoBehaviour
{
    [SerializeField] float existenceTime;

    float liftingSpeed = 100;
    Text text;


    private void Awake()
    {
        text = GetComponent<Text>();
        Destroy(gameObject, existenceTime);
    }

    void Update()
    {
        text.rectTransform.anchoredPosition += Vector2.up * liftingSpeed * Time.deltaTime;
        text.color = new Color(text.color.r, text.color.g, text.color.b
            , text.color.a - existenceTime * Time.deltaTime);
    }
}
