using System;
using UniRx;
using UnityEngine.UI;
using UnityEngine;

[DisallowMultipleComponent]
public class HitMarker : Singleton<HitMarker>
{
    public GlobalEvent<TerroristDeath> OnTerroristDeath = new GlobalEvent<TerroristDeath>();
    public GlobalEvent<TerroristDeath> OnHit = new GlobalEvent<TerroristDeath>();

    [SerializeField] private GameObject hitMarker = null, killMarker = null;
    [SerializeField] private GameObject canvas = null;
    [SerializeField] private float hitMarkerDisableDelay = 0.3f;

    private Camera uiCamera = null;

    private void Start()
    {
        uiCamera = Camera.main;

        OnHit.Subscribe(OnHitting);
        OnTerroristDeath.Subscribe(OnKill);
    }

    private void OnHitting(TerroristDeath target)
    {
        SetMarker(target, hitMarker);
    }

    private void OnKill(TerroristDeath target)
    {
        SetMarker(target, killMarker);
    }

    private void SetMarker(TerroristDeath target, GameObject marker)
    {
        Vector3 uiPosition = uiCamera.WorldToScreenPoint(target.HitPosition.transform.position);
        GameObject hitMark = Instantiate(marker, uiPosition, Quaternion.identity);
        hitMark.transform.SetParent(transform);

        Observable.Timer(TimeSpan.FromSeconds(hitMarkerDisableDelay)).TakeUntilDisable(gameObject)
            .Subscribe(g => { Destroy(hitMark); });
    }
}
