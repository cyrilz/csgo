using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class TapToInvokeSMTH : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] private GameObject level;
    [SerializeField] private string textOnButton;
    [SerializeField] private TMP_Text buttonText;

    private void Start()
    {
        buttonText.text = textOnButton;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        switch (level.name)
        {
            case "Level_1_WithAK":
                level.GetComponent<Level_1_Script>().TapToTake();
                break;
            case "Level_4_WithShield":
                level.GetComponent<Level_4_Script>().TapToTake();
                break;
        }

        gameObject.SetActive(false);
    }

}
