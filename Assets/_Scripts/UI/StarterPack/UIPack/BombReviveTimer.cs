using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BombReviveTimer : MonoBehaviour
{
    public Action OnTimerRestart;
    public Action OnTimerResume;
    public Action OnTimerStopped;
    public Action OnTimerEnded;
    
    [SerializeField] private float initialTime;
    [SerializeField] private Image fillImage;
    [SerializeField] private TextMeshProUGUI timeText;

    private float timer;
    private bool isStopped;
    private bool isEnded;

    public float Timer => timer;

    public bool IsEnded => isEnded;

    private void Start()
    {
        RestartTimer();
    }

    private void Update()
    {
        if (isEnded || isStopped)
            return;

        timer -= Time.deltaTime;
        if (timer > 0)
        {
            if (fillImage)
            {
                fillImage.fillAmount = timer / initialTime;
            }
            
            timeText.text = "00:" + (timer > 9? "" : "0") + CMath.Digits[(int) Mathf.Ceil(timer)];
        }
        else
        {
            if (fillImage)
            {
                fillImage.fillAmount = 0;
            }
            timeText.text = "00:0" + CMath.Digits[0];
            isEnded = true;
            OnTimerEnded?.Invoke();
        }

        if (timer <= 3)
            timeText.color = Color.red;
    }

    public void ResumeTimer()
    {
        isStopped = false;
        OnTimerResume?.Invoke();
    }

    public void StopTimer()
    {
        isStopped = true;
        OnTimerStopped?.Invoke();
    }

    public void RestartTimer()
    {
        OnTimerRestart?.Invoke();
        timer = initialTime;
        if (fillImage)
        {
            fillImage.fillAmount = 1;
        }
        
        timeText.text = "00:" + CMath.Digits[(int) Mathf.Ceil(initialTime)];
        isEnded = false;
    }

    private void OnDisable()
    {
        timeText.text = "00:00";
        timeText.color = Color.red;
    }
}