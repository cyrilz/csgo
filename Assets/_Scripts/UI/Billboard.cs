using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    [SerializeField] private bool isInclined;
    [SerializeField] private Transform target;

    private float currentZ = 0f;
    private Quaternion startRotation;

    private void Start()
    {
        if (isInclined)
        {
            currentZ = 340f;
            transform.localScale = new Vector3(0.22f, 0.22f, 1f);
            transform.localPosition = new Vector3(-0.143f, 2.582f, -0.43f);

            transform.forward = Camera.main.transform.forward;
            transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y, currentZ);

            startRotation = transform.localRotation;
            target = transform.parent;
            transform.parent = null;
        }
    }

    private void LateUpdate()
    {
        if (isInclined)
        {
            if (target)
            {
                transform.position = target.position + new Vector3(-0.143f, 2.582f, -0.43f);
                transform.localRotation = startRotation;
            }
            else Destroy(gameObject);
        }
        else
        {
            transform.forward = Camera.main.transform.forward;
            transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y, currentZ);
        }
    }
}