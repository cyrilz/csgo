using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class SignOverHead : MonoBehaviour
{
    private IDisposable signRotation = null;

    private void Start()
    {
        Quaternion previousRotation = Quaternion.identity;
        float timer = Time.time;

        transform.LookAt(PlayerController.Instance.transform.position);

        signRotation = Observable.EveryUpdate().TakeUntilDisable(gameObject)
            .Subscribe(x =>
            {
                transform.LookAt(PlayerController.Instance.transform.position);
                transform.localEulerAngles = new Vector3(transform.localEulerAngles.x,
                    transform.localEulerAngles.y - 180f, transform.localEulerAngles.z);
                if (Time.time - timer >= 2)
                {
                    if (previousRotation == transform.rotation)
                        signRotation.Dispose();
                    else
                    {
                        previousRotation = transform.rotation;
                        timer = Time.time;
                    }
                }
            });
    }

    //private void Swing()
    //{
    //    int direction = -1;
    //    Observable.EveryFixedUpdate().TakeWhile(g => gameObject.activeInHierarchy)
    //        .Subscribe(x =>
    //        {
    //            //if (Vector3.Distance(transform.localEulerAngles, targetEulerAngles) <= 0.2f)
    //            //{
    //            //    direction *= -1;
    //            //    targetEulerAngles.z *= direction;
    //            //}

    //            if(transform.localEulerAngles.z >= 9.9f && transform.localEulerAngles.z < 20f)
    //            {
    //                targetEulerAngles.z *= direction;
    //            }
    //            else if(transform.localEulerAngles.z <= 351.1f && transform.localEulerAngles.z > 340f)
    //            {
    //                targetEulerAngles.z *= direction;
    //            }
    //            transform.localEulerAngles = Vector3.MoveTowards(transform.localEulerAngles, targetEulerAngles, Time.fixedDeltaTime * 10);
    //        });
    //}
}
