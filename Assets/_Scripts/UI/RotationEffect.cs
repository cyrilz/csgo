using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationEffect : MonoBehaviour
{
    [SerializeField] private Transform _gameObjectRotate;
    [SerializeField] private float _speedRotation;
	[SerializeField] private Vector3 _vectorRotation;
	private void Start()
	{
		_gameObjectRotate = GetComponent<Transform>();
	}
	private void Update()
	{
		Rotate();
	}

	private void Rotate() => _gameObjectRotate.Rotate(_vectorRotation * _speedRotation * Time.deltaTime);
}
