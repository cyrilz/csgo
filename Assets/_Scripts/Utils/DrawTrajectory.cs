using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawTrajectory : MonoBehaviour
{
    LineRenderer lineRenderer;

    [SerializeField] private int trajectoryLenght;
    [SerializeField] private float distanceBetweenPoint;
    [SerializeField] private LayerMask leyers;
    [SerializeField] private Grenade grenade;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        DrawLine();
    }

    public void DrawLine() {
        lineRenderer.widthMultiplier = 0.02f;

        lineRenderer.positionCount = trajectoryLenght;
        List<Vector3> points = new List<Vector3>();
        Vector3 startingVelosity = transform.forward * grenade.throwingForce;
        for (float i = 0; i < trajectoryLenght; i += distanceBetweenPoint)
        {
            Vector3 newPoint = transform.position + i * startingVelosity;
            newPoint.y = transform.position.y + startingVelosity.y * i + Physics.gravity.y / 2 * i * i;

            if (newPoint.y < 0)
                break;

            points.Add(newPoint);
            if (Physics.OverlapSphere(newPoint, 0.02f, leyers).Length > 0) {
                lineRenderer.positionCount = points.Count;
                break;
            }
        }

        DrawLastTarget(points, grenade.LastTargetPoint);
        lineRenderer.SetPositions(points.ToArray());
    }

    private void DrawLastTarget(List<Vector3> points, GameObject target) // На сцене нужно создать пол и тогда всё будет работать
	{
        var lastPoint = points[points.Count-1];
        target.SetActive(true);
        target.transform.rotation = Quaternion.Euler(90, 0, 0);
        target.transform.position = new Vector3(lastPoint.x, 6f, lastPoint.z);
    }
}