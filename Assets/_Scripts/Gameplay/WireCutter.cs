using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireCutter : MonoBehaviour
{
    [SerializeField]private LayerMask substrate;
    [SerializeField] private LayerMask wire;

    private Vector3 lastHit;
    private bool hasPreviousHit = false;

    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, substrate))
            {
                RaycastHit wireHit;
                Vector3 direction = lastHit - hit.point;
                if (Physics.Raycast(hit.point, direction, out wireHit, direction.magnitude, wire)) {
                    Destroy(wireHit.collider.gameObject);
                }
            }

            hasPreviousHit = true;
            lastHit = hit.point;
        }
        else {
            hasPreviousHit = false;
        }
    }
}
