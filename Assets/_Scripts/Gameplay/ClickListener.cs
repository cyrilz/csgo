using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickListener : MonoBehaviour
{
    public Shooting shooting;
    public ShieldHolder shieldHolder;
    public float aimingDepth = 1000;

    [SerializeField] private Vector2 verticalRotation;
    [SerializeField] private Vector2 horizontalRotation;
    [SerializeField] private LayerMask bullet;

    private float verticalRot, horizontalRot;

    private const int ZERO = 0;
    private const float MIN_HOLD_TIME = 0.1f;
    private float currnetHoldTime;

    private float delayShot = 0;

    public float DelayShot { set { delayShot = value; } }

    private void Update()
    {
        Vector2 clickPosition = Vector2.zero;

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            clickPosition = touch.position;
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (shieldHolder.enabled == true
                && shieldHolder.shieldState == false)
            {
                StartCoroutine(Shoot());
            }
        }

        if (Input.GetAxis("Fire1") > 0)
        {
            clickPosition = Input.mousePosition;
        }

        if (clickPosition != Vector2.zero && shieldHolder.enabled == false)
        {
            currnetHoldTime += Time.deltaTime;

            Ray ray = Camera.main.ScreenPointToRay(clickPosition);

            Vector3 target = Physics.Raycast(ray, out RaycastHit hit, 1000, ~bullet) ? hit.point : ray.direction * 1000 + Camera.main.transform.position;

            shooting.Shoot(target, true);

            if (shooting.hands.currentWeapon != null)
            {
                Vector2 moveOffset = JoystickMerge.Instance.GetTapDelta();

                shooting.hands.currentWeapon.transform.Rotate(-moveOffset.y / 20, moveOffset.x / 20, 0);

                float x = shooting.hands.currentWeapon.transform.localEulerAngles.x;
                float y = shooting.hands.currentWeapon.transform.localEulerAngles.y;

                verticalRot = Mathf.Clamp(x - (x > 150 ? 360 : 0), verticalRotation.x, verticalRotation.y);
                horizontalRot = Mathf.Clamp(y - (y > 150 ? 360 : 0), horizontalRotation.x, horizontalRotation.y);

                shooting.hands.currentWeapon.transform.localEulerAngles = new Vector3(verticalRot, horizontalRot, ZERO);
            }
        }
        else
        {
            currnetHoldTime = 0;
        }
    }

    IEnumerator Shoot()
    {
        shieldHolder.ChangeShieldState();
        Vector3 target = shieldHolder.closestEnemy.position + new Vector3(0f, 1f, 0f);
        Quaternion previousRotation = Quaternion.identity;

        yield return new WaitForSeconds(0.2f);

        shooting.hands.currentWeapon.transform.LookAt(target);

        shooting.Shoot(target, true);
        shieldHolder.Invoke("ChangeShieldState", 0.2f);
    }
}
