using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

public class Level_5_Script_New : Level
{
    [HideInInspector] public Knife currentKnife;

    [SerializeField] private TextMeshProUGUI tapToStartText;
    [SerializeField] private GameObject sliderCanvas;

    [SerializeField] private Transform terrorist;
    [SerializeField] private Vector3 terroristTargetEulerAngles;

    [SerializeField] private Knife knife;
    [SerializeField] private GameObject ak47;

    [SerializeField] private Transform targetToTerror;
    [SerializeField] private Transform targetToKnife;
    [SerializeField] private GameObject noAmmoText;

    private bool lookAtPlayer;
    private bool lookInTerrorist = false;
    private bool goInTerrorist = false;
    private int countHits = 0;
    private AK47 _currentAK;

    NavMeshAgent playerNavMeshAgent;
    Hands hands;

    public int CountHits => countHits;

    private void Start()
    {
        Camera.main.transform.localPosition = new Vector3(0.02f, 1.5f, -0.07f);

        AimSights.Instance.AimToCenter = true;

        playerNavMeshAgent = player.GetComponent<NavMeshAgent>();
        hands = player.GetComponent<Hands>();

        EventManager.OnStartGame.Subscribe(MoveToKnife);

        playerNavMeshAgent.enabled = true;
        player.transform.SetPositionAndRotation(playerStartPosition.position, playerStartPosition.rotation);
        player.GetComponent<ClickListener>().enabled = false;

        terrorist.GetComponent<TerroristDeath>().Health = 100;
        hands.SetNewWeapon(ak47);
        _currentAK = hands.GetCurrentWeapon().GetComponent<AK47>();
        player.GetComponent<Shooting>().EnableCrosshair(true,false);
    }

    private void Update()
    {
        if (!sliderCanvas.activeInHierarchy && terrorist != null && Vector3.Distance(player.transform.position, terrorist.position) <= 2f)
        {
            sliderCanvas.SetActive(true);
            currentKnife.StartIdleAnimation();
        }

        if (terrorist == null)
        {
            Invoke(nameof(EndLevel), 2);
            enabled = false;
            sliderCanvas.SetActive(false);
        }

        if (lookInTerrorist)
        {
            if (Input.GetMouseButtonDown(0) && !goInTerrorist)
            {
                lookInTerrorist = false;
                goInTerrorist = true;

                //AimSights.Instance.gameObject.SetActive(false);
                player.GetComponent<Shooting>().EnableCrosshair(false,false);
                noAmmoText.SetActive(false);

                player.GetComponent<Hands>().ThrowWeapon();

                Camera.main.transform.localPosition = new Vector3(-0.02f, 1.5f, -0.07f);

                player.GetComponent<Hands>().SetNewWeapon(knife.gameObject);
                currentKnife = hands.GetComponentInChildren<Knife>();
                currentKnife.StartWalkingAnimation();

                Observable.EveryFixedUpdate().TakeUntilDisable(gameObject)
                    .TakeWhile(x => Vector3.Distance(player.transform.position, targetToTerror.position) >= 1.5f)
                    .Subscribe(s =>
                    {
                        PlayerController.Instance.MovePlayerToTarget(targetToTerror, Vector3.zero, playerNavMeshAgent.speed, Time.fixedDeltaTime);
                        PlayerController.Instance.RotatePlayerToTarget(terrorist, Vector3.zero, 0.5f);
                    });
            }
        }

        if (lookAtPlayer)
        {
            terrorist.GetComponent<TerroristLogic>().TurnOnPlayer();
            if (sliderCanvas.activeInHierarchy) sliderCanvas.SetActive(false);

            if (Mathf.Abs(terrorist.localEulerAngles.y - terroristTargetEulerAngles.y) <= 10f)
            {
                lookAtPlayer = false;
                terrorist.GetComponent<TerroristShooting>().AttackPlayer();
                player.GetComponent<PlayerDeath>().GetHit(player.GetComponent<CapsuleCollider>(), Vector3.zero, 0.8f);
                enabled = false;
            }
        }
    }

    private void MoveToKnife()
    {
        _currentAK.StartWalkingAnimation();
        playerNavMeshAgent.enabled = false;
        Vector3 offset = new Vector3(0f, 0f, 6.55f);
        //playerNavMeshAgent.SetDestination(targetToKnife.position);
        Observable.EveryFixedUpdate().TakeUntilDisable(gameObject)
            .TakeWhile(x => Vector3.Distance(player.transform.position, targetToKnife.position) >= 1.5f)
            .Finally(() =>
            {
                if (noAmmoText)
                {
                    _currentAK.StartIdleAnimation();
                    lookInTerrorist = true;
                    noAmmoText.SetActive(true);
                }
            })
            .Subscribe(z =>
            {
                PlayerController.Instance.MovePlayerToTarget(targetToKnife, Vector3.zero, playerNavMeshAgent.speed, Time.fixedDeltaTime);

                PlayerController.Instance.RotatePlayerToTarget(terrorist, offset,
                    Vector3.Distance(player.transform.position, targetToKnife.position) <= 5.5f ? 0.7f : 0.2f);
            });
    }

    public void HitEnemy(int damage)
    {
        if (terrorist == null)
            return;

        countHits++;
        currentKnife.Damage = damage;
        currentKnife.Attack(terrorist.position);

        if (countHits == 2 && terrorist.GetComponent<TerroristDeath>()?.Health > 0)
        {
            Observable.Timer(TimeSpan.FromSeconds(0.4f))
                .Subscribe(x =>
                {
                    lookAtPlayer = true;
                });
        }
    }

    private void EndLevel()
    {
        EndGameStatus endGameStatus = new EndGameStatus
        {
            addedCoins = 0,
            win = true
        };
        LevelManager.Instance.EndLevel(endGameStatus);
    }
}
