using System;
using System.Collections;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

public class Level3 : Level
{
    private NavMeshAgent playerNavMeshAgent;

    [SerializeField] private Transform playerFinalPosition;

    [SerializeField] private GameObject weaponAwp;
    [SerializeField] private GameObject weaponAk;

    [SerializeField] private GameObject terrorist;
    [SerializeField] private GameObject terroristRagdoll;

    [SerializeField] private GameObject girl;

    //[SerializeField] private GameObject girlModel;
    //[SerializeField] private RuntimeAnimatorController girlController;

    private AK47 _currentAK;
    private float terrorAttackDelay = 3f;

    private void Awake()
    {
        playerNavMeshAgent = player.GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        EventManager.OnStartGame.Subscribe(Activate);
        player.GetComponent<Shooting>().EnableCrosshair(false, false);
        Camera.main.transform.localPosition = new Vector3(0.02f, 1.5f, -0.07f);
        //Time.timeScale = 1;

        playerNavMeshAgent.enabled = false;

        player.transform.SetPositionAndRotation(playerStartPosition.position, playerStartPosition.rotation);

        player.GetComponent<ClickListener>().enabled = false;
        player.GetComponent<Hands>().SetNewWeapon(weaponAwp);
        player.GetComponent<CapsuleCollider>().isTrigger = true;
    }

    public void Activate()
    {
        player.GetComponent<Hands>().ThrowWeapon();

        //Destroy(weaponAwp);

        SetWeaponAK();

        player.GetComponent<Shooting>().EnableCrosshair(true,false);
        playerNavMeshAgent.updateRotation = false;
        playerNavMeshAgent.enabled = true;
        playerNavMeshAgent.speed = 5.5f;
        playerNavMeshAgent.destination = playerFinalPosition.position;

        //girlModel.GetComponent<Animator>().runtimeAnimatorController = girlController;
        girl.GetComponentInChildren<Animator>().enabled = true;

        StartCoroutine(ActivateWeapon());
    }

    IEnumerator ActivateWeapon()
    {
        _currentAK.StartWalkingAnimation();

        yield return new WaitForSeconds(1.5f);

        //Observable.EveryFixedUpdate().TakeUntilDisable(terrorist)
        //  .Subscribe(z => PlayerController.Instance.RotatePlayerToTarget(terrorist.transform, Vector3.zero, 1f));
        TurnOnRotation(new Vector3(0.334f,200f,-0.01f));

        yield return new WaitForSeconds(1.5f);

        CheckForEndGame();
        player.GetComponent<ClickListener>().enabled = true;
        player.GetComponent<Shooting>().EnableMoveCrosshair(true);

        Quaternion previousRotation = Quaternion.identity;
        playerNavMeshAgent.enabled = false;

        Observable.EveryFixedUpdate().TakeUntilDisable(gameObject)
            .TakeWhile(g =>
            {
                if (previousRotation == player.transform.rotation) return false;
                previousRotation = player.transform.rotation;
                return true;
            })
            .Finally(() => //activate terrorists && question sign = true && start rotation them
            {
                //terrorist.GetComponent<TerroristShooting>().PlayerDetected = true;
                Observable.Timer((terrorAttackDelay / 6).sec()).TakeUntilDisable(terrorist).Subscribe(_ => {
                    terrorist.GetComponent<TerroristShooting>().AddQuestionMark(); //0.5
                });

                Observable.Timer((terrorAttackDelay / 2).sec()).TakeUntilDisable(terrorist).Subscribe(_ => {
                    terrorist.GetComponent<TerroristShooting>().AddQuestionMark(); //половина времени
                });

                Observable.Timer((terrorAttackDelay / 6 * 5).sec()).TakeUntilDisable(terrorist).Subscribe(_ => {
                    terrorist.GetComponent<TerroristShooting>().AddQuestionMark(); //0.5 до выстрела
                });
                terrorist.GetComponent<TerroristShooting>().Invoke("AttackPlayer", terrorAttackDelay);

                player.GetComponent<CapsuleCollider>().isTrigger = false;
            })
            .Subscribe(x => //rotate player to target 
            {
                PlayerController.Instance.MovePlayerToTarget(playerFinalPosition.transform, Vector3.zero, 2f, Time.deltaTime);
                _currentAK.StartIdleAnimation();
            });

        //player.gameObject.transform.DORotate(new Vector3(0, 180), 1.5f)
        //    .OnComplete(() =>
        //    {
        //        //player.gameObject.transform.rotation = playerFinalPosition.rotation;

        //        terrorist.GetComponent<TerroristShooting>().PlayerDetected = true;
        //        terrorist.GetComponent<TerroristShooting>().Invoke("AttackPlayer", 2f);


        //        player.GetComponent<CapsuleCollider>().isTrigger = false;
        //        //StartSlowMo();
        //    });
    }

    private void SetWeaponAK()
    {
        Hands hands = player.GetComponent<Hands>();
        hands.SetNewWeapon(weaponAk);
        _currentAK = hands.GetCurrentWeapon().GetComponent<AK47>();
    }

    private void CheckForEndGame()
    {
        IDisposable checkEndGame = null;

        checkEndGame = Observable.EveryUpdate().TakeUntilDisable(gameObject)
            .SkipWhile(w =>
            {
                if (terrorist != null && girl != null)
                    return true;

                return false;
            })
            .Subscribe(x =>
            {
                //StopSlowMo();
                player.GetComponent<ClickListener>().enabled = false;

                if (girl != null)
                {
                    girl.GetComponentInChildren<Animator>().enabled = true;
                    girl.GetComponent<HostageDeath>().StandUp();
                }

                Observable.Timer(TimeSpan.FromSeconds(2.654f)).TakeUntilDisable(gameObject)
                .Subscribe(m =>
                {
                    EndLevel();
                    enabled = false;
                });

                checkEndGame.Dispose();
            });
    }

    private void TurnOnRotation(Vector3 rotation)
    {
        Observable.EveryUpdate().TakeUntil(Observable.Timer(1.7.sec())).Subscribe(_ =>
        {
            player.transform.rotation = Quaternion.Lerp(player.transform.rotation, Quaternion.Euler(rotation), Time.deltaTime * 1f);
        });
    }
    private void StartSlowMo()
    {
        Time.timeScale = 0.3f;
    }

    private void StopSlowMo()
    {
        Time.timeScale = 1;
    }
    

    private void EndLevel()
    {
        EndGameStatus endGameStatus = new EndGameStatus();
        endGameStatus.win = terrorist == null && girl != null;
        endGameStatus.addedCoins = 0;
        Singleton<LevelManager>.Instance.EndLevel(endGameStatus);

        player.GetComponent<ClickListener>().enabled = true;
        //StopSlowMo();
    }
}
