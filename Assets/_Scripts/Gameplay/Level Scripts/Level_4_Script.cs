using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;
using UniRx;

public class Level_4_Script : Level
{
    [SerializeField] private Transform lookWeapon;
    [SerializeField] private Transform playerAttackPosition;
    [SerializeField] private Transform playerFinalPosition;

    [SerializeField] private GameObject ak47Prefab;
    [SerializeField] private GameObject girl;
    [SerializeField] private GameObject pistolInScene;
    [SerializeField] private GameObject pistolPrefab;
    [SerializeField] private GameObject shieldInScene;
    [SerializeField] private GameObject shieldPrefab;
    [SerializeField] private GameObject tapToTakeButton;
    [SerializeField] private GameObject thiefDeadPrefab;

    [SerializeField] private Transform pistolInSceneTransform;
    [SerializeField] private Transform shieldInSceneTransform;
    [SerializeField] private Transform thiefDeadTransform;

    [SerializeField] private ClickListener clickListener;
    [SerializeField] private List<Transform> terrorists;
    //[SerializeField] private RuntimeAnimatorController girlController;

    [SerializeField] private float shootInterval = 3;

    private const bool DEACTIVATED = false;
    
    private Dictionary<float, Transform> distancesToEnemy;
    private NavMeshAgent playerNavMeshAgent;
    private Hands hands;
    private ShieldHolder shieldHolder;
    private AK47 _currentAK;
    private IDisposable goToShield = null, waitDistance = null, moveToEndPos = null;

    private static bool isActivated = false;
    
    private Vector3 pistolDefaultLocalPosition = new Vector3(0.0766f, 0.08f, 0.1676f);
    private Vector3 pistolDefaultLocalEulerAngles = new Vector3(29f, -1.71f, -2.75f);

    private void Awake()
    {
        playerNavMeshAgent = player.GetComponent<NavMeshAgent>();
        hands = player.GetComponent<Hands>();
        shieldHolder = player.GetComponent<ShieldHolder>();
    }

    private void SpawnPlayer(Transform spawnPosition)
    {
        playerNavMeshAgent.enabled = false;
        player.transform.position = spawnPosition.position;
        player.transform.rotation = spawnPosition.rotation;
    }
    
    private void Start()
    {
        Camera.main.transform.localPosition = new Vector3(0.02f, 1.5f, -0.07f);

        player.GetComponent<ClickListener>().enabled = false;
        CountingDistancesToEnemies();

        Instantiate(thiefDeadPrefab, thiefDeadTransform.position, thiefDeadPrefab.transform.rotation, transform);
        girl.GetComponentInChildren<Animator>().enabled = true;
        girl.GetComponent<HostageDeath>().IdledUp();
        //girl.GetComponentInChildren<Animator>().runtimeAnimatorController = girlController;

        SetShieldAndPistolTransforms();

        if (!isActivated)
        {
            EventManager.OnStartGame.Subscribe(Activate);

            hands.SetNewWeapon(ak47Prefab);
            _currentAK = hands.GetCurrentWeapon().GetComponent<AK47>();

            player.GetComponent<Shooting>().EnableCrosshair(true, false);
            AimSights.Instance.AimToCenter = true;

            SpawnPlayer(playerStartPosition);
        }
        else
        {
            SpawnPlayer(playerAttackPosition);
            player.transform.Rotate(0, -176.311f, 0);
            TapToTake(false);
        }
    }

    public void TapToTake(bool isFirstPlay = true)
    {
        if(hands.currentWeaponObj) player.GetComponent<Hands>().ThrowWeapon();

        Destroy(shieldInScene);
        Destroy(pistolInScene);

        hands.SetNewShield(shieldPrefab);
        hands.SetNewWeapon(pistolPrefab);
        hands.currentWeaponObj.transform.localPosition = pistolDefaultLocalPosition;
        hands.currentWeaponObj.transform.localEulerAngles = pistolDefaultLocalEulerAngles;
        hands.currentWeaponObj.transform.localScale = new Vector3(2f, 2f, 2f);

        player.GetComponent<ShieldHolder>().enabled = true;

        if (isFirstPlay)
        {
            StartCoroutine(ActivateWeapon(1.8f));
            ActiveEmeny();

            playerNavMeshAgent.enabled = true;
            playerNavMeshAgent.destination = playerAttackPosition.position;
        }
        else
            EventManager.OnStartGame.Subscribe(() => 
            {
                StartCoroutine(ActivateWeapon(0.1f));
                ActiveEmeny();

                playerNavMeshAgent.enabled = true;
                playerNavMeshAgent.destination = playerAttackPosition.position;
            });
    }

    public void Activate()
    {
        _currentAK.StartWalkingAnimation();

        playerNavMeshAgent.enabled = true;
        playerNavMeshAgent.speed = 4.5f;
        playerNavMeshAgent.destination = playerFinalPosition.position;

        goToShield = Observable.EveryUpdate().TakeUntilDisable(gameObject).Where(w =>
        {
            if (Vector3.Distance(player.transform.position, playerFinalPosition.position) < 1.6f)
                return true;
            return false;
        })
            .Subscribe(x =>
            {
                if (moveToEndPos != null)
                {
                    moveToEndPos.Dispose();
                    playerNavMeshAgent.updateRotation = true;
                }

                Quaternion previousRotation = Quaternion.identity;
                Observable.EveryFixedUpdate()
                    .TakeWhile(g =>
                    {
                        if (previousRotation == player.transform.rotation) return false;
                        previousRotation = player.transform.rotation;
                        return true;
                    })
                    .Finally(() => 
                    {
                        player.GetComponent<Shooting>().EnableCrosshair(false, false);
                        Destroy(shieldInScene);
                        Destroy(pistolInScene);

                        tapToTakeButton.SetActive(true);
                    })
                    .Subscribe(x =>
                    {
                        PlayerController.Instance.RotatePlayerToTarget(lookWeapon, Vector3.down, 2.5f);
                        isActivated = true;
                    });
                goToShield.Dispose();
            });

        waitDistance = Observable.EveryFixedUpdate().TakeUntilDisable(gameObject)
            .Where(w =>
            {
                if (Vector3.Distance(player.transform.position, playerAttackPosition.position) <= 12f)
                    return true;

                return false;
            })
            .Subscribe(x=> 
            {
                waitDistance.Dispose();
                playerNavMeshAgent.updateRotation = false;

                moveToEndPos =  Observable.EveryFixedUpdate().TakeUntilDisable(gameObject)
                .Subscribe(x =>
                {
                    PlayerController.Instance.RotatePlayerToTarget(playerAttackPosition, Vector3.down, 3f);
                });
            });
    }
    
    private void Update()
    {
        if (distancesToEnemy.Count == 0)
        {
            clickListener.enabled = DEACTIVATED;
            Invoke(nameof(EndLevel), 2);
            enabled = false;
        }

        if(shieldHolder.closestEnemy == null && distancesToEnemy.Count != 0)
        {
            ChooseNewClosestEnemy();
        }

        if (Vector3.Distance(player.transform.position, playerAttackPosition.position) < 0.1f) 
        {
            playerNavMeshAgent.enabled = false;
        }
    }
    
    private void ChooseNewClosestEnemy()
    {
        distancesToEnemy.Remove(distancesToEnemy.Keys.First());

        if (distancesToEnemy.Count > 0)
        {
            shieldHolder.closestEnemy = distancesToEnemy[distancesToEnemy.Keys.First()];
        }
    }

    private void CountingDistancesToEnemies()
    {
        distancesToEnemy = new Dictionary<float, Transform>();

        for(int i = 0; i < terrorists.Count; i++)
        {
            Transform terrorist = terrorists[i];
            terrorist.GetComponent<TerroristShooting>().RepeatTime = shootInterval;
            distancesToEnemy.Add(Vector3.Distance(playerStartPosition.localPosition, terrorist.localPosition), terrorist);
        }

        distancesToEnemy = distancesToEnemy.OrderBy(pair => pair.Key).ToDictionary(pair => pair.Key, pair => pair.Value);
        shieldHolder.closestEnemy = distancesToEnemy[distancesToEnemy.Keys.First()];
    }

    private void EndLevel()
    {
        EndGameStatus endGameStatus = new EndGameStatus();
        endGameStatus.addedCoins = 0;
        endGameStatus.win = true;
        Singleton<LevelManager>.Instance.EndLevel(endGameStatus);
    }

    private void ActiveEmeny()
    { 
        for(int i = 0; i < terrorists.Count; i++)
        {
            terrorists[i].GetComponent<TerroristShooting>().enabled = true;
        }
    }

    IEnumerator ActivateWeapon(float delay)
    {
        yield return new WaitForSeconds(delay);

        player.GetComponent<ClickListener>().enabled = true;
    }

    private void SetShieldAndPistolTransforms()
    {
        shieldInScene.transform.localPosition = shieldInSceneTransform.localPosition;
        shieldInScene.transform.localRotation = shieldInSceneTransform.localRotation;
        shieldInScene.transform.localScale = shieldInSceneTransform.localScale;

        pistolInScene.transform.localPosition = pistolInSceneTransform.localPosition;
        pistolInScene.transform.localRotation = pistolInSceneTransform.localRotation;
        pistolInScene.transform.localScale = pistolInSceneTransform.localScale;
    }
}