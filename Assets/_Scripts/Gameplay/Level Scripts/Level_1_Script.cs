using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

public class Level_1_Script : Level
{
    [SerializeField] private TapToInvokeSMTH tapToTakeButton;
    [SerializeField] private Transform stage2Transform;
    [SerializeField] private GameObject startGameMenu;

    [SerializeField] private Transform lookPoint;

    [SerializeField] private GameObject ak47InScene;
    [SerializeField] private GameObject emptyHands;
    [SerializeField] private GameObject ak47Prefab;
    [SerializeField] private GameObject awpPrefab;

    [SerializeField] private GameObject ak47InCanvas;
    [SerializeField] private GameObject awpInCanvas;

    [SerializeField] private Transform terroristTargetPoint_1;
    [SerializeField] private TerroristLogic terrorist_1;

    [SerializeField] private Transform terroristTargetPoint_2;
    [SerializeField] private TerroristLogic terrorist_2;

    [SerializeField] private float stopRotationTime;

    private bool takingAwp = false;
    private IDisposable terroristsRotation = null, firstShot = null,
        shootPlayer = null, waitKillTer1 = null, waitKillTer2 = null, waitTakeWeapon = null;
    private NavMeshAgent playerNavMeshAgent;
    private GameObject droppedWeapon;
    private TerroristMoveController terrorist_1_MC, terrorist_2_MC;
    private TerroristShooting terrorist_1_SH, terrorist_2_SH;

    private static bool isFirstTry = true;

    private const float HAND_TO_AK_TIME = 0.533f;

    private AK47 currentAK;

    private void Awake()
    {
        playerNavMeshAgent = player.GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        Camera.main.transform.localPosition = new Vector3(0.02f, 1.5f, -0.07f);

        AimSights.Instance.gameObject.SetActive(false);

        terrorist_1_MC = terrorist_1.GetComponent<TerroristMoveController>();
        terrorist_2_MC = terrorist_2.GetComponent<TerroristMoveController>();
        terrorist_1_SH = terrorist_1.GetComponent<TerroristShooting>();
        terrorist_2_SH = terrorist_2.GetComponent<TerroristShooting>();

        if (isFirstTry)
        {
            player.GetComponent<Hands>().SetNewWeapon(emptyHands);
            EventManager.OnStartGame.Subscribe(Activate);

            SetPlayerStartState(playerStartPosition);
        }
        else
        {
            SetPlayerStartState(stage2Transform);
            player.transform.rotation = Quaternion.Euler(-3.481f, 119.653f, -0.616f);
            startGameMenu.SetActive(true);

            SetWeaponAK();

            EventManager.OnStartGame.Subscribe(Activate);
        }
    }

    public void Activate()
    {
        Vector3 offset = new Vector3(0.1f, -2f, 0f);
        Observable.EveryFixedUpdate()
            .TakeWhile(w =>
            {
                if (Vector3.Distance(player.transform.position, ak47InScene.transform.position) <= 2f) return false;
                return true;
            })
            .Finally(() => //enable tap to take button
            {
                if (isFirstTry)
                {
                    player.GetComponent<Hands>().currentWeapon.GetComponent<EmptyHands>().PlayAnimationHandToAK();

                    Observable.Timer(TimeSpan.FromSeconds(HAND_TO_AK_TIME)).TakeUntilDestroy(gameObject)
                    .Subscribe(g =>
                    {
                        tapToTakeButton.gameObject.SetActive(true);
                        tapToTakeButton.enabled = true;
                    });

                    Observable.Timer(TimeSpan.FromSeconds(HAND_TO_AK_TIME / 2f)).TakeUntilDestroy(gameObject)
                   .Subscribe(g =>
                   {
                       Destroy(ak47InScene);
                   });
                }
                else
                {
                    TapToTake();
                }
            })
            .Subscribe(x => //move to ak
            {
                PlayerController.Instance.MovePlayerToTarget(ak47InScene.transform, Vector3.zero, 3.5f, Time.fixedDeltaTime);
                PlayerController.Instance.RotatePlayerToTarget(ak47InScene.transform, offset, 2.5f);
            });
    }

    public void TapToTake()
    {
        if (takingAwp)
        {
            currentAK.StartIdleAnimation();
            player.GetComponent<Hands>().SetNewWeapon(awpPrefab);
            EndLevel();
            return;
        }

        if (isFirstTry)
        {
            Destroy(player.GetComponent<Hands>().GetCurrentWeapon());

            SetWeaponAK();
        }

        isFirstTry = false;

        AimSights.Instance.gameObject.SetActive(true);

        Quaternion previousRotation = Quaternion.identity;
        Vector3 offset = new Vector3(0f, 0.3f, 0f);

        player.GetComponent<Shooting>().EnableCrosshair(true, true);

        terrorist_1.Activate();
        terrorist_2.Activate();

        Observable.EveryFixedUpdate().TakeUntilDisable(gameObject)
            .TakeWhile(g =>
            {
                if (previousRotation == player.transform.rotation) return false;
                previousRotation = player.transform.rotation;
                return true;
            })
            .Finally(() => //activate terrorists && question sign = true && start rotation them
            {
                CheckForKillTerrorist();
                ActivateWeapon();
                TerroristsTurnOnPlayer();
            })
            .Subscribe(x => //rotate player to target 
            {
                PlayerController.Instance.RotatePlayerToTarget(lookPoint, offset, 2.5f);
            });
    }

    private void SetPlayerStartState(Transform point)
    {
        playerNavMeshAgent.enabled = false;

        player.transform.position = point.position;
        player.transform.eulerAngles = point.eulerAngles;

        player.GetComponent<ClickListener>().enabled = false;
    }

    private void SetWeaponAK()
    {
        Hands hands = player.GetComponent<Hands>();
        hands.SetNewWeapon(ak47Prefab);
        currentAK = hands.GetCurrentWeapon().GetComponent<AK47>();
        currentAK.bulletSpeedMult = 2;
    }

    private void TerroristsTurnOnPlayer()
    {
        float timer = 0f;

        terroristsRotation = Observable.EveryUpdate().TakeUntilDisable(gameObject)
            .SkipWhile(w => //waiting for stop both terrorists && start checking on death && activate weapon
            {
                //turn terrorist
                if (!terrorist_1_MC.IsMoving)
                    terrorist_1.TurnOnRotation(terroristTargetPoint_1.transform.eulerAngles);
                if (!terrorist_2_MC.IsMoving)
                    terrorist_2.TurnOnRotation(terroristTargetPoint_1.transform.eulerAngles);

                if (terrorist_1 == null && terrorist_2 == null)
                    terroristsRotation.Dispose();

                if ((!terrorist_1_MC.IsMoving && !terrorist_2_MC.IsMoving) ||
                (!terrorist_2_MC.IsMoving && terrorist_1 == null) ||
                (!terrorist_1_MC.IsMoving && terrorist_2 == null))
                {
                    timer = Time.time;
                    return false;
                }
                return true;
            })
            .Subscribe(x => //rotating terrorists to player
            {
                if (terrorist_1)
                {
                    if (terrorist_1_SH.Suspicion == false)
                        terrorist_1_SH.Suspicion = true;

                    //terrorist_1.TurnOnPlayer();
                    terrorist_1.TurnOnRotation(terroristTargetPoint_1.transform.eulerAngles);
                }

                if (terrorist_2)
                {
                    if (terrorist_2_SH.Suspicion == false)
                        terrorist_2_SH.Suspicion = true;

                    //terrorist_2.TurnOnPlayer();
                    terrorist_2.TurnOnRotation(terroristTargetPoint_2.transform.eulerAngles);
                }

                float checkTimeScale = Time.timeScale == 1f ? stopRotationTime : stopRotationTime / 3;
                if (Time.time - timer >= checkTimeScale) //stop rotating after "stopRotationTime" && start shooting
                {
                    if (terrorist_1) terrorist_1_SH.PlayerDetected = true;
                    if (terrorist_2) terrorist_2_SH.PlayerDetected = true;
                    terroristsRotation.Dispose();

                    ShootPlayer();
                }
            });
    }

    private void OnDisable()
    {
        Time.timeScale = 1;
    }

    private void EndLevel()
    {
        EndGameStatus endGameStatus = new EndGameStatus
        {
            addedCoins = 0,
            win = true
        };
        LevelManager.Instance.EndLevel(endGameStatus);
    }

    private void ActivateWeapon()
    {
        if (player == null) return;
        player.GetComponent<ClickListener>().enabled = true;

        firstShot = Observable.EveryUpdate().TakeUntilDisable(gameObject)
            .Where(w => //waiting first shot
            {
                if (Input.GetAxis("Fire1") > 0) return true;
                return false;
            })
            .Subscribe(x => // if shot, time slows down after 0.15 seconds
            {
                Observable.Timer(TimeSpan.FromSeconds(0.15f)).TakeUntilDisable(gameObject).Subscribe(x => Time.timeScale = 0.3f);

                firstShot.Dispose();
            });

        player.GetComponent<Animator>().runtimeAnimatorController = null;
        //player.transform.LookAt(lookPoint);

        //cam.GetComponent<Animator>().runtimeAnimatorController = null;
        //cam.transform.rotation = Quaternion.Euler(0, 0, 0);

    }

    private void CheckForKillTerrorist()
    {
/*        waitKillTer1 = Observable.EveryUpdate().TakeUntilDisable(gameObject)
            .Where(w => terrorist_1 != null && terrorist_2 == null).Subscribe(x =>
            { // checks for death of second terrorist and triggers first one in case of death
                terroristsRotation.Dispose();
                terrorist_1_SH.PlayerDetected = true;
                StartCoroutine(WaitAndShootT1());

                waitKillTer1.Dispose();
                waitKillTer2.Dispose();
            });

        waitKillTer2 = Observable.EveryUpdate().TakeUntilDisable(gameObject)
            .Where(w => terrorist_2 != null && terrorist_1 == null).Subscribe(x =>
            { // checks for death of first terrorist and triggers second in case of death
                terroristsRotation.Dispose();
                terrorist_2_SH.PlayerDetected = true;
                terrorist_2_MC.StopMoving();
                ShootPlayer();

                waitKillTer1.Dispose();
                waitKillTer2.Dispose();
            });*/

        waitTakeWeapon = Observable.EveryUpdate().TakeUntilDisable(gameObject)
            .SkipWhile(w =>
            { // waiting when both terrorists dead
                if (terrorist_1 == null && terrorist_2 == null)
                {
                    player.GetComponent<ClickListener>().enabled = false;
                    player.GetComponent<Shooting>().EnableMoveCrosshair(false);
                    return false;
                }
                return true;
            })
            .Subscribe(x =>
            {
                Time.timeScale = 1f;
                droppedWeapon = GameObject.Find("Sniper(Clone)");
                Vector3 offset = new Vector3(0f, -2f, 0f);

                Observable.EveryFixedUpdate().TakeUntilDisable(gameObject)
                .TakeWhile(w =>
                {
                    if (Vector3.Distance(player.transform.position, droppedWeapon.transform.position) <= 3.5f)
                    {
                        return false;
                    }
                    return true;
                })
                .Finally(() =>
                { // take weapon && invoke WIN after 1 second
                    if (player)
                    {
                        Destroy(droppedWeapon);

                        player.GetComponent<Shooting>().EnableCrosshair(false, false);
                        player.GetComponent<Hands>().ThrowWeapon();

                        ak47InCanvas.SetActive(false);
                        awpInCanvas.SetActive(true);

                        tapToTakeButton.gameObject.SetActive(true);
                        takingAwp = !takingAwp;
                    }
                })
                .Subscribe(x =>
                { // move && rotate player to dropped weapon
                    currentAK.StartWalkingAnimation();
                    PlayerController.Instance.MovePlayerToTarget(droppedWeapon.transform, Vector3.zero, 6f, Time.fixedDeltaTime);
                    PlayerController.Instance.RotatePlayerToTarget(droppedWeapon.transform, offset, 4f);
                });

                waitTakeWeapon.Dispose();
            });
    }

    private void ShootPlayer(float delay = 1)
    {
        if (shootPlayer == null)
        {
            shootPlayer = Observable.EveryUpdate()
                .TakeUntil(Observable.Timer(TimeSpan.FromSeconds(delay)))
                .Finally(() =>
                {
                    if (terrorist_1)
                    {
                        terrorist_1_SH.InitBulletPosition();
                        terrorist_1_SH.AttackPlayer();
                    }

                    if (terrorist_2)
                    {
                        terrorist_2_SH.InitBulletPosition();
                        terrorist_2_SH.AttackPlayer();
                    }

                    shootPlayer.Dispose();
                })
                .Subscribe(x =>
                {
                    if (terrorist_1)
                    {
                        if (terrorist_1.TerroristSpeedRotation != 10)
                            terrorist_1.TerroristSpeedRotation = 10;

                        terrorist_1.TurnOnPlayer();
                    }

                    if (terrorist_2)
                    {
                        if (terrorist_2.TerroristSpeedRotation != 10)
                            terrorist_2.TerroristSpeedRotation = 10;

                        terrorist_2.TurnOnPlayer();
                    }
                });
        }
    }

    private IEnumerator WaitAndShootT1()
	{
        while(terrorist_1_MC.IsMoving)
        {
            yield return null;
        }
        //yield return new WaitForSeconds(0.55f);
        terrorist_1_MC.StopMoving();
        ShootPlayer(0.1f);
    }
}
