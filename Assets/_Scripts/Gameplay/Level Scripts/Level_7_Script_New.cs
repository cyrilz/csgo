using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

public class Level_7_Script_New : Level
{
    [SerializeField] private TextMeshProUGUI tapToStartText;
    [SerializeField] private RuntimeAnimatorController cameraAnimation;
    [SerializeField] private GameObject wire1;
    [SerializeField] private GameObject wire2;
    [SerializeField] private GameObject bombExplodeVFX;
    [SerializeField] private BombReviveTimer timer;
    [SerializeField] private List<GameObject> wrongWires = new List<GameObject>();
    [SerializeField] private GameObject door;
    [SerializeField] private MeshRenderer bombMeshRenderer;
    [SerializeField] private GameObject bomb;
    [SerializeField] private GameObject kusachki;
    [SerializeField] private Vector3 offsetKusachki;
    [SerializeField] private float timeUntilExplode = 1.0f;

    [SerializeField] private Transform overBombTransform;

    private bool isEnd;
    private Camera cam;
    private NavMeshAgent playerNavMeshAgent;
    private IDisposable bombLamp;

    private const float TIME_UNTIL_LEVEL_FAIL = 1.0f;

    private void Awake()
    {
        cam = Camera.main;
        playerNavMeshAgent = player.GetComponent<NavMeshAgent>();
    }


    private void Start()
    {
        //AimSights.Instance.gameObject.SetActive(false);
        player.GetComponent<Shooting>().EnableCrosshair(false,false);
        EventManager.OnStartGame.Subscribe(Activate);
        timer.enabled = true;
        timer.StopTimer();

        timer.OnTimerEnded += LoseLevel;
        playerNavMeshAgent.enabled = false;

        player.transform.SetPositionAndRotation(playerStartPosition.position, playerStartPosition.rotation);

        tapToStartText.text = "Defuse the bomb";
    }

    public void Activate()
    {
        //StartCoroutine(Open());

        Observable.Timer(2.2f.sec()).TakeUntilDisable(gameObject).Subscribe(c => cam.GetComponent<Animator>().runtimeAnimatorController = cameraAnimation);

        Observable.EveryUpdate().TakeUntilDisable(gameObject)
            .TakeWhile(z => Vector3.Distance(player.transform.position, playerStartPosition.position) < 6.0f)
            .Finally(() =>
            {
                kusachki.SetActive(true);
                //kusachki.transform.position = player.transform.position + player.transform.forward * offsetKusachki.z + Vector3.up * offsetKusachki.y + player.transform.right * offsetKusachki.x;
                timer.ResumeTimer();

                Quaternion previousRotation = Quaternion.identity;
                Observable.EveryFixedUpdate().TakeWhile(w =>
                {
                    if (previousRotation == player.transform.rotation) return false;
                    previousRotation = player.transform.rotation;
                    return true;
                })
                .Subscribe(x =>
                {
                    player.transform.position = Vector3.MoveTowards(player.transform.position, overBombTransform.position, Time.fixedDeltaTime * 2.5f);
                    player.transform.rotation = Quaternion.Lerp(player.transform.rotation, overBombTransform.rotation, Time.fixedDeltaTime * 2.5f);
                });

                Observable.EveryUpdate().TakeWhile(z => door.transform.localRotation.y < 125).TakeUntilDisable(gameObject)
                .Subscribe(x =>
                {
                    door.transform.localRotation = Quaternion.Lerp(door.transform.localRotation, Quaternion.Euler(0, 130, 0), 3f * Time.deltaTime);
                });

                bombLamp = Observable.Interval(1f.sec()).TakeUntilDisable(gameObject).Subscribe(x =>
                {
                    if (timer.Timer > 0)
                    {
                        bombMeshRenderer.materials[9].color = Color.green;
                        Observable.Timer(0.5f.sec()).TakeUntilDisable(gameObject).Subscribe(z => bombMeshRenderer.materials[9].color = Color.white);
                    }
                    else
                    {
                        bombMeshRenderer.materials[9].color = Color.red;
                        Observable.Timer(0.5f.sec()).Subscribe(z => bombMeshRenderer.materials[9].color = Color.white);
                    }
                });
            })
            .Subscribe(x =>
            {
                player.transform.position += player.transform.forward * Time.deltaTime * 2.5f;
            });

    }

    private void Update()
    {
        if (isEnd)
            return;

        if (wire1 == null && wire2 == null)
        {
            timer.StopTimer();
            Invoke(nameof(EndLevel), 0.5f);
            enabled = false;
            isEnd = true;
        }
        if (WrongWireCunt())
        {
            timer.enabled = false;
            Invoke(nameof(LoseLevel), 0.5f);
            isEnd = true;
        }
    }

    private void EndLevel()
    {
        EndGameStatus endGameStatus = new EndGameStatus
        {
            addedCoins = 0,
            win = true
        };
        Singleton<LevelManager>.Instance.EndLevel(endGameStatus);
    }

    private void LoseLevel()
    {
        bombMeshRenderer.materials[9].color = Color.red;
        bombLamp.Dispose();

        bombLamp = Observable.Interval(0.3f.sec()).TakeUntilDisable(gameObject)
            .Subscribe(x =>
            {
                bombMeshRenderer.materials[9].color = Color.red;
                Observable.Timer(0.15f.sec()).Subscribe(z => bombMeshRenderer.materials[9].color = Color.white);
            });

        Kusachki.Instance.enabled = false;
        EndGameStatus endGameStatus = new EndGameStatus
        {
            addedCoins = 0,
            win = false
        };

        Observable.Timer(TimeSpan.FromSeconds(timeUntilExplode)).TakeUntilDisable(gameObject)
            .Subscribe(x =>
            {
                kusachki.SetActive(false);
                bomb.SetActive(false);
                bombExplodeVFX.SetActive(true);
            });

        Observable.Timer(TimeSpan.FromSeconds(timeUntilExplode + TIME_UNTIL_LEVEL_FAIL)).TakeUntilDisable(gameObject)
            .Subscribe(x => LevelManager.Instance.EndLevel(endGameStatus));
    }

    private void OnDestroy()
    {
        if (Camera.main != null)
        {
            Camera.main.GetComponent<Animator>().runtimeAnimatorController = null;
        }
    }

    private bool WrongWireCunt()
    {
        foreach (var item in wrongWires)
        {
            if (item == null)
            {
                return true;
            }
        }
        return false;
    }


    //IEnumerator Open()
    //{

    //    //while (door.transform.localEulerAngles.y > -180)
    //    //{
    //    //    Debug.Log("Works");
    //    //    //yield return new WaitForSeconds(0.3f);
    //    //    door.transform.localRotation = Quaternion.LookRotation(Vector3.RotateTowards(door.transform.localEulerAngles, new Vector3(0, -180,0), 0.5f,0.0f));
    //    //    //door.transform.rotation = transform.RotateAround()//Quaternion.Euler(Vector3.Ra (door.transform.rotation.eulerAngles, new Vector3(-90, 0 - 90), 5, 5));
    //    //    //transform.RotateAround(door.transform.position,, 20 * Time.deltaTime);
    //    //}
    //}
}
