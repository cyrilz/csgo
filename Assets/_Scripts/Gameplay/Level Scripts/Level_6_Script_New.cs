using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;
using UniRx;

public class Level_6_Script_New : Level
{
    [SerializeField] private Transform levelEndPosition = null;
    [SerializeField] private Transform levelEndLookPosition = null;

    [SerializeField] private GameObject grenade;
    [SerializeField] private GameObject knife;

    [SerializeField] private Transform terrorist1;
    [SerializeField] private TerroristLogic terrorist1_logic;

    [SerializeField] private Transform terrorist2;
    [SerializeField] private TerroristLogic terrorist2_logic;

    [SerializeField] private Transform targetToTerrorists;
    [SerializeField] private Transform lookPoint;

    [SerializeField] private float agressiveDelay = 2.0f;

    private NavMeshAgent playerNavMeshAgent;
    private Hands hands;
    private Knife currentKnife;

    private const float ACCELERATION = 1.0f;

    private void Awake()
    {
        playerNavMeshAgent = player.GetComponent<NavMeshAgent>();
        hands = player.GetComponent<Hands>();
    }

    private void Start()
    {
        Camera.main.transform.localPosition = new Vector3(-0.02f, 1.5f, -0.07f);

        //AimSights.Instance.gameObject.SetActive(false);
        player.GetComponent<Shooting>().EnableCrosshair(false, false);
        playerNavMeshAgent.enabled = false;
        playerNavMeshAgent.speed = 5f;

        player.transform.position = playerStartPosition.position;
        player.transform.rotation = playerStartPosition.rotation;

        player.GetComponent<ClickListener>().enabled = false;
        hands.SetNewWeapon(knife);

        currentKnife = hands.GetCurrentWeapon().GetComponent<Knife>();

        EventManager.OnStartGame.Subscribe(MoveToTerrorists);
    }

    private void MoveToTerrorists()
    {
        currentKnife.StartWalkingAnimation();

        //playerNavMeshAgent.enabled = true;
        //playerNavMeshAgent.SetDestination(targetToTerrorists.position);

        Observable.EveryFixedUpdate().TakeUntilDisable(gameObject)
            .TakeWhile(w => Vector3.Distance(player.transform.position, targetToTerrorists.position) >= 3f)
            .Finally(() =>
            {
                Quaternion previousRotation = Quaternion.identity;

                Observable.EveryFixedUpdate().TakeUntilDisable(gameObject)
                    .TakeWhile(g =>
                    {
                        if (previousRotation == player.transform.rotation) return false;

                        previousRotation = player.transform.rotation;
                        return true;
                    })
                    .Finally(() => //need take grenade
                    {
                        if (hands)
                        {
                            Destroy(hands.currentWeaponObj);
                            currentKnife.StartIdleAnimation();
                            Camera.main.transform.localPosition = new Vector3(-0.06f, 1.65f, 0.03f);
                            hands.SetNewWeapon(grenade);
                        }
                    })
                    .Subscribe(x => //rotate player to target 
                    {
                        PlayerController.Instance.RotatePlayerToTarget(lookPoint, Vector3.zero, 2.5f);
                    });
            })
            .Subscribe(x =>
            {
                PlayerController.Instance.MovePlayerToTarget(targetToTerrorists, Vector3.zero, playerNavMeshAgent.speed, Time.fixedDeltaTime);
                PlayerController.Instance.RotatePlayerToTarget(lookPoint, Vector3.zero, 2.5f);
            });
    }

    public void TriggerTerroristAggressive()
    {
        Observable.EveryUpdate().TakeUntilDisable(gameObject)
            .TakeUntil(Observable.Timer(TimeSpan.FromSeconds(agressiveDelay)))
            .Finally(() => //wait seconds for rotate and then kill player
            {
                Observable.Timer(TimeSpan.FromSeconds(0.05)).TakeUntilDisable(gameObject)
                .Subscribe(z =>
                {
                    if (terrorist1) terrorist1.GetComponent<TerroristShooting>().AttackPlayer();

                    if (terrorist2) terrorist2.GetComponent<TerroristShooting>().AttackPlayer();

                    if (terrorist1 || terrorist2)
                    {
                        PlayerController.Instance.GetComponent<PlayerDeath>()
                            .GetHit(PlayerController.Instance.GetComponent<CapsuleCollider>(), Vector3.zero, 1.5f);
                    }
                    else
                    {
                        //MoveToEndOfLevel();
                        Invoke(nameof(EndLevel), 1.2f);
                    }
                });
            })
            .Subscribe(x =>
            {
                if (terrorist1) terrorist1_logic.TurnOnPlayer();
                if (terrorist2) terrorist2_logic.TurnOnPlayer();
            });
    }

    private void EndLevel()
    {
        EndGameStatus endGameStatus = new EndGameStatus
        {
            addedCoins = 0,
            win = true
        };
        LevelManager.Instance.EndLevel(endGameStatus);
    }

    private void MoveToEndOfLevel()
    {
        Observable.EveryUpdate().TakeUntilDisable(gameObject)
            .TakeWhile(w => Vector3.Distance(player.transform.position, levelEndPosition.position) >= 0.05f)
            .Finally(() => { Invoke(nameof(EndLevel), 0.5f); })
            .Subscribe(h =>
            {
                PlayerController.Instance.MovePlayerToTarget(levelEndPosition, Vector3.zero, playerNavMeshAgent.speed + ACCELERATION, Time.deltaTime);
                PlayerController.Instance.RotatePlayerToTarget(levelEndLookPosition, Vector3.zero, 3.5f);
            });
    }
}
