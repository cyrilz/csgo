using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

public class Level_2_Script : Level
{
    [SerializeField]
    private TextMeshProUGUI tapToStartText;
    [SerializeField] private GameObject sliderCanvas;
    
    [HideInInspector] public Knife currentKnife;

    [SerializeField]
    private Transform terrorist;
    [SerializeField] private Vector3 terroristTargetEulerAngles;
    private bool lookAtPlayer;
    private int countHits = 0;

    [SerializeField] private GameObject knife;
    [SerializeField] private GameObject ak47;

    [SerializeField] private Transform targetToTerror;
    [SerializeField] private Transform targetToKnife;
    [SerializeField] private GameObject noAmmoText;

    private bool lookInTerrorist = false;
    private bool goInTerrorist = false;

    NavMeshAgent playerNavMeshAgent;
    Hands hands;

    public int CountHits => countHits;
    private void Awake()
    {
        playerNavMeshAgent = player.GetComponent<NavMeshAgent>();
        hands = player.GetComponent<Hands>();
    }

    private void Start()
    {
        EventManager.OnStartGame.Subscribe(MoveToKnife);

        playerNavMeshAgent.enabled = true;
        player.transform.position = playerStartPosition.position;
        player.transform.rotation = playerStartPosition.rotation;
        player.GetComponent<ClickListener>().enabled = false;

        terrorist.GetComponent<TerroristDeath>().Health = 100;
        hands.SetNewWeapon(ak47);
    }

    private void Update()
    {
        if (!sliderCanvas.activeInHierarchy && terrorist != null && Vector3.Distance(player.transform.position, terrorist.position) <= 2f)
        {
            sliderCanvas.SetActive(true);
        }

        if (terrorist == null)
        {
            Invoke("EndLevel", 2);
            enabled = false;
        }

        if (lookInTerrorist)
        {
            Vector3 direction = targetToTerror.position - player.transform.position;
            Quaternion rotation = Quaternion.LookRotation(direction);
            player.transform.rotation = Quaternion.Lerp(player.transform.rotation, rotation, 2f * Time.deltaTime);

            if (Input.GetMouseButtonDown(0) && !goInTerrorist)
            {
                lookInTerrorist = false;
                goInTerrorist = true;
                noAmmoText.SetActive(false);
                player.GetComponent<Hands>().ThrowWeapon();
                player.GetComponent<Hands>().SetNewWeapon(knife);

                currentKnife = hands.GetComponentInChildren<Knife>();

                playerNavMeshAgent.enabled = true;
                playerNavMeshAgent.SetDestination(targetToTerror.position);

                Observable.EveryUpdate().TakeWhile(x => Vector3.Distance(player.transform.position, targetToTerror.position) >= 1.5f).Finally(() =>
                {
                    lookInTerrorist = true;
                    playerNavMeshAgent.enabled = false;
                }).Subscribe();
            }
        }
    }

    private void MoveToKnife()
    {
        playerNavMeshAgent.SetDestination(targetToKnife.position);
        Observable.EveryUpdate().TakeWhile(x => Vector3.Distance(player.transform.position, targetToKnife.position) >= 1.5f).Finally(() =>
        {
            playerNavMeshAgent.enabled = false;
            lookInTerrorist = true;
            noAmmoText.SetActive(true);
        }).Subscribe(z =>
        {
            Vector3 direction = targetToKnife.position - player.transform.position;
            Quaternion rotation = Quaternion.LookRotation(direction);
            player.transform.rotation = Quaternion.Lerp(player.transform.rotation, rotation, 2f * Time.deltaTime);
        });
    }

    private void FixedUpdate()
    {
        if (lookAtPlayer)
        {
            terrorist.localEulerAngles = Vector3.Lerp(terrorist.localEulerAngles, terroristTargetEulerAngles, Time.fixedDeltaTime * 4);
            if (Vector3.Distance(terrorist.localEulerAngles, terroristTargetEulerAngles) <= 30f)
            {
                lookAtPlayer = false;
                terrorist.GetComponent<TerroristShooting>().AttackPlayer();
                player.GetComponent<PlayerDeath>().GetHit(player.GetComponent<CapsuleCollider>(), Vector3.zero, 0.5f);
                terrorist.GetComponent<TerroristShooting>().enabled = false;
                enabled = false;
            }
        }
    }

    public void HitEnemy(int damage)
    {
        countHits++;
        currentKnife.Damage = damage;
        currentKnife.Attack(terrorist.position);

        if (countHits == 2 && terrorist.GetComponent<TerroristDeath>()?.Health > 0)
        {
            Observable.Timer(TimeSpan.FromSeconds(0.4f)).Subscribe(x =>
            {
                lookAtPlayer = true;
                terrorist.GetComponent<TerroristShooting>().enabled = true;
            });
        }
    }

    private void EndLevel()
    {
        EndGameStatus endGameStatus = new EndGameStatus();
        endGameStatus.addedCoins = 0;
        endGameStatus.win = true;
        LevelManager.Instance.EndLevel(endGameStatus);
    }
}
