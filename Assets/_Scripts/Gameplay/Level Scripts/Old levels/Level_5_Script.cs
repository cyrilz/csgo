using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

public class Level_5_Script : Level
{
    [SerializeField]
    private Transform terrorist;

    [SerializeField] private Transform terroristTarget1 = null, terroristTarget2 = null;
    private int currentTarget = 1;

    [SerializeField]
    private GameObject startWeapon;

    [HideInInspector]
    internal bool isFirstShoot = false;
    private bool active = false;

    NavMeshAgent playerNavMeshAgent;
    Hands hands;

    private void Awake()
    {

        playerNavMeshAgent = player.GetComponent<NavMeshAgent>();
        player.GetComponent<ClickListener>().enabled = false;
        player.GetComponent<ZoomThenShot>().enabled = false;
    }

    private void Start()
    {
        EventManager.OnStartGame.Subscribe(Activate);
        playerNavMeshAgent.enabled = false;

        hands = player.GetComponent<Hands>();
        player.transform.position = playerStartPosition.position;
        player.transform.rotation = playerStartPosition.rotation;
        hands.SetNewWeapon(startWeapon);
        Invoke("Zoom", 1);
    }

    public void DeactivateZoomThenShot()
    {
        if (player == null)
        {
            return;
        }
        player.GetComponent<ClickListener>().enabled = true;
        player.GetComponent<ZoomThenShot>().enabled = false;

    }

    private void Update()
    {
        if (terrorist == null)
        {
            Invoke("DeactivateZoomThenShot", 2);
            Invoke("EndLevel", 2);
            enabled = false;
        }
        if (isFirstShoot && !active)
        {
            LoseLevel();
            active = true;
        }
    }

    public void EndLevel()
    {
        player.GetComponent<ZoomThenShot>().enabled = false;
        EndGameStatus endGameStatus = new EndGameStatus();
        endGameStatus.addedCoins = 0;
        endGameStatus.win = true;
        Singleton<LevelManager>.Instance.EndLevel(endGameStatus);
    }

    public void LoseLevel()
    {
        if (terrorist != null)
        {
            var terroristMoveContr = terrorist.GetComponent<TerroristMoveController>();
            terroristMoveContr.StopMoving();
            terroristMoveContr.enabled = false;

            Observable.Timer(TimeSpan.FromSeconds(1f)).Subscribe(x =>
            {
                var shotScript = terrorist.GetComponent<TerroristShooting>();
                shotScript.enabled = true;
                shotScript.AttackPlayer();
                player.GetComponent<ZoomThenShot>().enabled = false;
                enabled = false;
            });
            //EndGameStatus endGameStatus = new EndGameStatus();
            //endGameStatus.addedCoins = 0;
            //endGameStatus.win = false;
            //Singleton<LevelManager>.Instance.EndLevel(endGameStatus);
        }
    }

    private void Zoom()
    {
        player.GetComponent<ZoomThenShot>().enabled = true;
        player.GetComponent<ZoomThenShot>().Level = this;

    }

    private void Activate()
    {
        IDisposable terroristPatrol = null;
        terroristPatrol = Observable.Interval(TimeSpan.FromSeconds(5f)).TakeUntilDisable(gameObject).Subscribe(x =>
        {
            if (terrorist) terrorist.GetComponent<TerroristMoveController>().SetDestination(currentTarget == 1 ? terroristTarget1.position : terroristTarget2.position);
            else terroristPatrol?.Dispose();

            currentTarget = currentTarget == 1 ? 2 : 1;
            if (enabled == false) terroristPatrol?.Dispose();
        });
    }
}
