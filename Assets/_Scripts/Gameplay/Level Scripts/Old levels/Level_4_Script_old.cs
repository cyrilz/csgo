﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Level_4_Script_old : Level
{
    NavMeshAgent playerNavMeshAgent;

    [SerializeField]
    private Animator doorAnim;

    [SerializeField]
    private Transform playerFinalPosition;

    [SerializeField]
    private GameObject weapon;

    [SerializeField]
    private GameObject terrorist;

    [SerializeField]
    private GameObject girl;
    private void Awake()
    {
        playerNavMeshAgent = player.GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        EventManager.OnStartGame.Subscribe(Activate);

        playerNavMeshAgent.enabled = false;
        player.transform.position = playerStartPosition.position;
        player.transform.rotation = playerStartPosition.rotation;

        player.GetComponent<ClickListener>().enabled = false;

        player.GetComponent<Hands>().SetNewWeapon(weapon);
    }

    public void Activate()
    {
        playerNavMeshAgent.enabled = true;
        playerNavMeshAgent.destination = playerFinalPosition.position;

        StartCoroutine(ActivateWeapon());

        doorAnim.Play("Open");
        Invoke("StartSlowMo", 1.4f);

        terrorist.GetComponent<TerroristShooting>().Invoke("AttackPlayer", 2.5f);

    }

    private void Update()
    {
        if (terrorist == null || girl == null)
        {
            Invoke("EndLevel", 2);
            enabled = false;
        }
    }

    private void EndLevel()
    {
        EndGameStatus endGameStatus = new EndGameStatus();
        endGameStatus.win = terrorist == null && girl != null;
        endGameStatus.addedCoins = 0;
        Singleton<LevelManager>.Instance.EndLevel(endGameStatus);

        player.GetComponent<ClickListener>().enabled = true;
        StopSlowMo();
    }

    IEnumerator ActivateWeapon()
    {
        yield return new WaitForSeconds(1);
        player.GetComponent<ClickListener>().enabled = true;
    }

    private void StartSlowMo()
    {
        Time.timeScale = 0.3f;
    }

    private void StopSlowMo()
    {
        Time.timeScale = 1;
    }

    private void OnDisable()
    {
        StopSlowMo();
    }
}
