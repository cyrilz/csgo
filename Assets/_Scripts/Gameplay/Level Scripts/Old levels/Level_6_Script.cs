using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class Level_6_Script : Level
{
    [SerializeField] private RuntimeAnimatorController cameraAnimation;

    [SerializeField] private GameObject shieldInScene;

    [SerializeField] private GameObject ak47Prefab;
    [SerializeField] private GameObject shieldPrefab;
    [SerializeField] private ClickListener clickListener;
    [SerializeField] private List<Transform> terrorists;
    [SerializeField] private Dictionary<float, Transform> distancesToEnemy;

    private const bool DEACTIVATED = false;

    private NavMeshAgent playerNavMeshAgent;
    private Camera cam;
    private Hands hands;
    private ShieldHolder shieldHolder;
    public float shootInterval = 3;

    private void Awake()
    {
        cam = Camera.main;
        playerNavMeshAgent = player.GetComponent<NavMeshAgent>();
        hands = player.GetComponent<Hands>();
        shieldHolder = player.GetComponent<ShieldHolder>();
    }

    private void Start()
    {
        EventManager.OnStartGame.Subscribe(Activate);

        CountingDistancesToEnemies();

        playerNavMeshAgent.enabled = false;

        player.transform.position = playerStartPosition.position;
        player.transform.rotation = playerStartPosition.rotation;

        hands.SetNewWeapon(ak47Prefab);
        hands.currentWeaponObj.transform.rotation = Quaternion.Euler(30f, 0f, 0f);

        player.GetComponent<ClickListener>().enabled = false;
        player.GetComponent<ShieldHolder>().enabled = true;

        cam.transform.rotation = Quaternion.Euler(50, 0, 0);
    }

    public void Activate()
    {
        Destroy(shieldInScene);
        hands.SetNewShield(shieldPrefab);
        cam.GetComponent<Animator>().runtimeAnimatorController = cameraAnimation;

        StartCoroutine(ActiveEmeny());
        StartCoroutine(ActivateWeapon());
    }

    private void Update()
    {
        if (distancesToEnemy.Count == 0)
        {
            clickListener.enabled = DEACTIVATED;
            Invoke(nameof(EndLevel), 2);
            enabled = false;
        }

        if(shieldHolder.closestEnemy == null && distancesToEnemy.Count != 0)
        {
            ChooseNewClosestEnemy();
        }
    }

    private void ChooseNewClosestEnemy()
    {
        distancesToEnemy.Remove(distancesToEnemy.Keys.First());
        if (distancesToEnemy.Count > 0)
        {
            shieldHolder.closestEnemy = distancesToEnemy[distancesToEnemy.Keys.First()];
        }
    }

    private void CountingDistancesToEnemies()
    {
        distancesToEnemy = new Dictionary<float, Transform>();

        for(int i = 0; i < terrorists.Count; i++)
        {
            Transform terrorist = terrorists[i];
            terrorist.GetComponent<TerroristShooting>().RepeatTime = shootInterval;
            distancesToEnemy.Add(Vector3.Distance(playerStartPosition.localPosition, terrorist.localPosition), terrorist);
        }

        distancesToEnemy = distancesToEnemy.OrderBy(pair => pair.Key).ToDictionary(pair => pair.Key, pair => pair.Value);
        shieldHolder.closestEnemy = distancesToEnemy[distancesToEnemy.Keys.First()];
    }

    private void EndLevel()
    {
        EndGameStatus endGameStatus = new EndGameStatus();
        endGameStatus.addedCoins = 0;
        endGameStatus.win = true;
        Singleton<LevelManager>.Instance.EndLevel(endGameStatus);
    }

    IEnumerator ActiveEmeny()
    {
        yield return new WaitForSeconds(0.2f);
        for(int i = 0; i < terrorists.Count; i++)
        {
            terrorists[i].GetComponent<TerroristShooting>().enabled = true;
        }
    }

    IEnumerator ActivateWeapon()
    {
        yield return new WaitForSeconds(2);
        player.GetComponent<ClickListener>().enabled = true;

        cam.GetComponent<Animator>().runtimeAnimatorController = null;
        cam.transform.rotation = Quaternion.Euler(0, 0, 0);
    }
}
