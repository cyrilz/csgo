using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelListUI : MonoBehaviour
{
    LevelManager levelManager;

    [SerializeField] private GameObject levelsCanvas;
    [SerializeField] private GameObject buttonPrefab;
    private List<Button> buttons;

    private void Awake()
    {
        levelManager = GetComponent<LevelManager>();
    }

    private void Start()
    {
        buttons = new List<Button>();
        for (int i = 0; i < levelManager.levels.Length; i++)
        {
            GameObject newButton = Instantiate(buttonPrefab, levelsCanvas.transform);
            newButton.transform.localPosition += new Vector3(0, -20 * i,0);
            Button newButtonComponent = newButton.GetComponent<Button>();
            buttons.Add(newButtonComponent);
            buttons[i].onClick.AddListener(() => { ChangeLevel(buttons.IndexOf(newButtonComponent)); });
            newButton.GetComponentInChildren<Text>().text = levelManager.levels[i].name;
        }
    }

    public void ChangeLevel(int level) {

        EndGameStatus endGameStatus = new EndGameStatus();
        endGameStatus.addedCoins = 0;
        endGameStatus.win = false;
        Singleton<LevelManager>.Instance.EndLevel(endGameStatus,true);

        levelManager.CurrentLevel.Value = level;
    }
}
