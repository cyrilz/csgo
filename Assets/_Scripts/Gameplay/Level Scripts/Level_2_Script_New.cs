using UnityEngine;
using UnityEngine.AI;
using UniRx;
using System;
using System.Collections.Generic;

[DisallowMultipleComponent]
public class Level_2_Script_New : Level
{
    [SerializeField] private GameObject sniperRifle = null;
    [SerializeField] private GameObject terrorist = null;
    [SerializeField] private Transform startPosition = null, endPosition = null;
    [SerializeField] private int attempts = 3;
    [SerializeField] private List<Transform> targets;

    private float delay = 1.0f, shotDelay = 0.6f, winDelay = 2.0f, moveDelay = 2f;
    private float remainingDistanceToPoint = 0.1f;

    private bool IsOnPointA;
    private int currentShot;
    private IDisposable onStartGame = null;
    private NavMeshAgent terroristMeshAgent = null;
    private TerroristShooting terroristShooting = null;
    private TerroristMoveController tMoveController = null;
    private SniperShoot sniperShoot;

    private void Start()
    {
        Camera.main.transform.localPosition = new Vector3(0.02f, 1.5f, -0.07f);

        AimSights.Instance.gameObject.SetActive(false);

        currentShot = 0;
        IsOnPointA = false;

        player.GetComponent<Hands>().SetNewWeapon(sniperRifle);
        player.transform.SetPositionAndRotation(playerStartPosition.position, playerStartPosition.rotation);

        terroristMeshAgent = terrorist.GetComponent<NavMeshAgent>();
        terroristShooting = terrorist.GetComponent<TerroristShooting>();
        tMoveController = terrorist.GetComponent<TerroristMoveController>();
        terroristShooting.ShootAtStart = false;
        terroristShooting.PlayerDetected = true;
        player.GetComponent<ClickListener>().enabled = false;
        sniperShoot = player.GetComponent<SniperShoot>();
        sniperShoot.enabled = true;
        EventManager.OnStartGame.Subscribe(OnStartGame);
    }

    private void Update()
    {

        if (terrorist == null)
        {
            Invoke(nameof(FinishLevel), winDelay);
            enabled = false;
        }
    }

    private void OnStartGame()
    {
        MoveTo(endPosition);
        Observable.Timer(moveDelay.sec()).TakeUntilDestroy(terrorist)
               .Subscribe(_ =>
               {
                   ShootAndMoveBack();
               });
    }

    private void ShootAndMoveBack()
    {
        //��������
        if (currentShot >= attempts)
        {
            terroristShooting.ShootOnTarget(PlayerController.Instance.transform, true);
        }
        else
        {
            Transform tempTarget = targets[UnityEngine.Random.Range(0, targets.Count)];

            terroristShooting.ShootOnTarget(tempTarget.transform, true);
            targets.Remove(tempTarget);
        }
        currentShot++;

        Observable.Timer(shotDelay.sec()).TakeUntilDestroy(terrorist)
            .Subscribe(h =>
            {
                //���� �����
                MoveTo(startPosition);
                Observable.Timer(moveDelay.sec()).TakeUntilDisable(terrorist)
                .Subscribe(z =>
                {
                    //���� �� ���
                    MoveTo(endPosition);
                    Observable.Timer(moveDelay.sec()).TakeUntilDisable(terrorist)
                    .Subscribe(x =>
                    {
                        //������, ��������� ��������
                        ShootAndMoveBack();
                    });
                });
            }).AddTo(gameObject);
    }

    private void MoveTo(Transform transform)
    {
        if (terrorist != null && terroristMeshAgent.isActiveAndEnabled &&
                                 terroristMeshAgent.isOnNavMesh &&
                                 !terroristMeshAgent.pathPending &&
                                 terroristMeshAgent.remainingDistance <= remainingDistanceToPoint)
        {
            tMoveController.SetDestination(transform.position);
        }
    }

    private void FinishLevel()
    {
        EndGameStatus endGameStatus = new EndGameStatus { win = true };
        LevelManager.Instance.EndLevel(endGameStatus);
    }
}
