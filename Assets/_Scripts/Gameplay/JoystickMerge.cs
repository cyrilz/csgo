﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoystickMerge : Singleton<JoystickMerge>, IDragHandler, IPointerUpHandler
{
    private Vector3 tapDelta;

    public void OnDrag(PointerEventData eventData)
    {
        if (eventData.IsPointerMoving())
            tapDelta = eventData.delta;
        else
            tapDelta = CMath.Zero;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        tapDelta = CMath.Zero;
    }

    public Vector3 GetTapDelta()
    {
        Vector3 res = tapDelta;
        tapDelta = CMath.Zero;

        return res;
    }

    //private void LateUpdate()
    //{
    //    tapDelta = CMath.Zero;
    //}
}
