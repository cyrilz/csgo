using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RunningSlider : MonoBehaviour
{
    [SerializeField] private Transform pointer;
    [SerializeField] private float pointerSpeed;
    [SerializeField] private Level_5_Script_New levelScript;

    [SerializeField] private GameObject damageText;

    private int direction = 1; //1 - right, -1 - left
    private float targetX = 212f;

    private bool waitForHit = false;

    //  ______________________________________________________________________
    // |  RED ZONE  |  YELLOW ZONE  |  GREEN ZONE  |  YELLOW ZONE  |  RED ZONE  |
    // |-212  -108.5|-108.5    -25.4|-25.4     25.4|25.4      108.5|108.5    212|
    // |____________|_______________|______________|_______________|____________|

    public enum Damage : int
    {
        High = 100,
        Medium = 65,
        Low = 35
    }

    private void Update()
    {
        if (waitForHit) return;

        var pointerX = pointer.localPosition.x;
        if (pointerX >= 211f) direction = -1;
        else if (pointerX <= -211f) direction = 1;

        var currentX = Mathf.MoveTowards(pointer.localPosition.x, targetX * direction, pointerSpeed * Time.deltaTime);
        pointer.localPosition = new Vector3(currentX, pointer.localPosition.y, pointer.localPosition.z);

        if (Input.GetMouseButtonDown(0))
        {
            int damage = 0;
            if (Mathf.Abs(pointerX) < 212f && Mathf.Abs(pointerX) > 108.5f) //red zone
            {
                damage = (int)Damage.Low;
            }
            else if (Mathf.Abs(pointerX) < 108.5f && Mathf.Abs(pointerX) > 25.4f) //yellow zone
            {
                damage = (int)Damage.Medium;
            }
            else if (pointerX < 25.4f && pointerX > -25.4f) //green zone
            {
                damage = (int)Damage.High;
            }

            if (levelScript.CountHits < 2)
            {
                if(!levelScript.currentKnife.TimeToShoot())
                {
                    return;
                }

                levelScript.HitEnemy(damage);
                waitForHit = true;
                StartCoroutine(ShowDamageOnScreen(damage));
            }
        }
    }

    IEnumerator ShowDamageOnScreen(float damage) 
    {
        yield return new WaitForSeconds(0.2f);

        Text text = Instantiate(damageText, gameObject.transform).GetComponent<Text>();
        text.rectTransform.anchoredPosition = new Vector2(Random.Range(-100, 100), Random.Range(-100, 100));

        if (damage == (int)Damage.High) 
        {
            text.color = Color.green;
            text.text = ((int)Damage.High).ToString();
        }
        else if (damage == (int)Damage.Medium)
        {
            text.color = Color.yellow;
            text.text = ((int)Damage.Medium).ToString();
        }
        else if (damage == (int)Damage.Low)
        {
            text.color = Color.red;
            text.text = ((int)Damage.Low).ToString();
        }

        waitForHit = false;
    }
}
