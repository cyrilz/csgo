using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomThenShot : MonoBehaviour
{
    public float mouseSensitivity;
    float xRotation = 0f;

    public Transform weaponZoomPosition;

    Hands hands;

    private float zoomStrength = 30;
    private float startTime;
    internal Level_5_Script Level;
    private Vector3 cameraStartLocalPosition;

    private bool _isZoomed;
    private bool shot = false;

    private void Start()
    {
        hands = GetComponent<Hands>();

        cameraStartLocalPosition = Camera.main.transform.localPosition;
        //startTime = Time.time;
        //hands.currentWeaponObj.transform.parent = weaponZoomPosition;
        //hands.currentWeaponObj.transform.localPosition = Vector3.zero;
        //hands.currentWeaponObj.transform.localRotation = Quaternion.identity;

        //if (!_isZoomed)
        //{
        //    Camera.main.transform.localPosition += new Vector3(0, 0, zoomStrength);
        //    _isZoomed = true;
        //}

    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {

            hands.currentWeaponObj.transform.parent = weaponZoomPosition;
            hands.currentWeaponObj.transform.localPosition = Vector3.zero;
            hands.currentWeaponObj.transform.localRotation = Quaternion.identity;

            if (!_isZoomed)
            {
                Camera.main.transform.localPosition += new Vector3(0, 0, zoomStrength);
                _isZoomed = true;
                startTime = Time.time;
            }
        }

        if (_isZoomed && Input.GetButton("Fire1") && Time.time - startTime> 0.7f)
        {
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
        
            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);
            Camera.main.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        
            transform.Rotate(Vector3.up * mouseX);
        }
        
        if (Input.GetButtonUp("Fire1") && Time.time - startTime > 0.7f && _isZoomed && !shot) {
            GetComponent<Shooting>().Shoot(hands.currentWeaponObj.transform.forward * 100000);
            Level.isFirstShoot = true;
            shot = true;
            Invoke("ReturnCamera", 1.2f);

           
        }

        
    }

    private void ReturnCamera()
    {
        hands.currentWeaponObj.transform.parent = hands.weaponPlaceHolder;
        hands.currentWeaponObj.transform.localPosition = Vector3.zero;
        hands.currentWeaponObj.transform.localRotation = Quaternion.identity;

        Camera.main.transform.localPosition = cameraStartLocalPosition;
        enabled = false;
    }
}
