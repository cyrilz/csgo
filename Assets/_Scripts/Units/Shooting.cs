using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    [SerializeField] private bool IsShooting = true;
    [SerializeField] private PlayerDeath playerDeath;
    [SerializeField] private GameObject crosshairParent;
    [SerializeField] private RectTransform crosshair;
    
    public Hands hands;

    private bool isMoveCrosshair;

    private Vector2 centerScreen;

    private void Awake()
    {
        centerScreen = new Vector2(Screen.width / 2, Screen.height / 2);
    }

    public void Shoot(Vector3 target, bool followingBullet = false) {
        if (hands.currentWeapon != null && IsShooting) {
            hands.currentWeapon.Attack(target, followingBullet);
        } 
    }
    public void EnableCrosshair(bool enable, bool isMove)
    {
        crosshairParent.SetActive(enable);
        isMoveCrosshair = isMove;
       // if (!isMove)
            //crosshair.anchoredPosition = centerScreen;
    }

    public void EnableMoveCrosshair(bool enable)
    {
        if (enable)
            isMoveCrosshair = true;
        else
        {
            isMoveCrosshair = false;
            //crosshair.anchoredPosition = centerScreen;
        }
    }

    private void Update()
    {
        if (Input.GetMouseButton(0) && crosshairParent.activeSelf && isMoveCrosshair)
        {
            //crosshair.anchoredPosition = (Vector2)Input.mousePosition;
        }
    }

    private void OnEnable()
    {
        playerDeath.Died += ChangeIsShooting;
    }

    private void OnDisable()
    {
        playerDeath.Died -= ChangeIsShooting;
    }

    private void ChangeIsShooting()
    {
        IsShooting = false;
    }

}
