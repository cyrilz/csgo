using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPoints : MonoBehaviour
{
    protected float healthPoints;

    public virtual void GetHit(Collider collider, Vector3 force, float time = 0, bool showDeathMarker = true) {

    }

    public virtual void GetHit(float damage, Collider collider, Vector3 force, bool showDeathMarker = true)
    {

    }
}
