using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class ShieldHolder : MonoBehaviour
{
    [SerializeField] GameObject shieldHolder;

    private Animator animator;
    private Hands hands;

    private Quaternion shieldStartRotation = Quaternion.identity;
    private Quaternion pistolStartRotation = Quaternion.identity;

    private IDisposable pistolRotating = null;

    [HideInInspector] public bool shieldState = false; //true-open, false-close
    [HideInInspector] public Transform closestEnemy;

    private void Start()
    {
        animator = shieldHolder.GetComponentInChildren<Animator>();
        hands = GetComponent<Hands>();
    }

    public void ChangeShieldState()
    {
        if (shieldStartRotation == Quaternion.identity)
        {
            shieldStartRotation = PlayerController.Instance.transform.rotation;
            pistolStartRotation = hands.currentWeaponObj.transform.rotation;
        }

        shieldState = !shieldState;

        if (shieldState)
        {
            RotatePlayerToTarget();
            animator?.SetTrigger("Active");
            //hands.currentShield.IsOpening = true;
        }
        else
        {
            //RotatePlayerToDefault();
            ChangeWeaponState();
            //hands.currentShield.IsClosing = true;
        }

    }

    private void ChangeWeaponState()
    {
        Quaternion previousRotation = Quaternion.identity;

        pistolRotating = Observable.EveryFixedUpdate().TakeUntilDestroy(closestEnemy.gameObject).TakeUntilDestroy(gameObject)
            .TakeWhile(w =>
            {
                if (previousRotation == hands.currentWeaponObj.transform.rotation) return false;
                previousRotation = hands.currentWeaponObj.transform.rotation;
                return true;
            })
            .Finally(() => 
            { 
                    hands.currentShield.IsClosing = true;
            })
            .Subscribe(x =>
            {
                hands.currentWeaponObj.transform.rotation = Quaternion.Lerp(hands.currentWeaponObj.transform.rotation
                    , pistolStartRotation, Time.fixedDeltaTime * 5);
            });
    }

    private void RotatePlayerToTarget()
    {
        Quaternion previousRotation = Quaternion.identity;

        pistolRotating?.Dispose();

        Observable.EveryFixedUpdate().TakeUntilDestroy(closestEnemy.gameObject).TakeUntilDestroy(gameObject)
            .TakeWhile(w =>
            {
                if (previousRotation == PlayerController.Instance.transform.rotation) return false;
                previousRotation = PlayerController.Instance.transform.rotation;
                return true;
            })
            .Finally(() => 
            { 
                hands.currentShield.IsOpening = true; 
            })
            .Subscribe(x =>
            {
                PlayerController.Instance.RotatePlayerToTarget(closestEnemy, Vector3.zero, 2.5f);
            });
    }

    private void RotatePlayerToDefault()
    {
        Quaternion previousRotation = Quaternion.identity;

        Observable.EveryFixedUpdate().TakeUntilDisable(gameObject)
            .TakeWhile(w =>
            {
                if (previousRotation == PlayerController.Instance.transform.rotation) return false;
                previousRotation = PlayerController.Instance.transform.rotation;
                return true;
            })
            .Finally(() => 
            { 
                hands.currentShield.IsClosing = true; 
            })
            .Subscribe(x =>
            {
                PlayerController.Instance.transform.rotation = Quaternion.Lerp(PlayerController.Instance.transform.rotation
                    , shieldStartRotation, Time.fixedDeltaTime * 5);
            });
    }
}
