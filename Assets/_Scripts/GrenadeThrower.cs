using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeThrower : MonoBehaviour
{
    [SerializeField] private GameObject grenadeProjectile;
    [SerializeField] private GameObject grenade;
    [SerializeField] private GameObject throwPivot;
    public float throwingForce;
    
    public void ThrowGrenade()
    {
        grenade.SetActive(false);
        GameObject projectile = Instantiate(grenadeProjectile, grenade.transform.position, grenade.transform.rotation);
        projectile.GetComponent<Rigidbody>().velocity = throwPivot.transform.forward * throwingForce;
        //Destroy(gameObject);
    }
}
