using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Singleton<PlayerController>
{
    public void MovePlayerToTarget(Transform target, Vector3 offset, float speed, float time)
    {
        transform.position = Vector3.MoveTowards(transform.position, target.position + offset, time * speed);
    }

    public void RotatePlayerToTarget(Transform target, Vector3 offset, float speed)
    {
        Vector3 playerDirection = (target.position + offset) - transform.position;
        Quaternion playerRotation = Quaternion.LookRotation(playerDirection);

        transform.rotation = Quaternion.Lerp(transform.rotation, playerRotation, Time.fixedDeltaTime * speed);
    }
}
