using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.Events;

public class PlayerDeath : HealthPoints
{
    public event UnityAction Died;
    
    public override void GetHit(Collider collider, Vector3 force, float time = 0, bool showHitMarker = true)
    {
        EndLevel(time);
    }

    private void EndLevel(float time = 0)
    {
        Died?.Invoke();
        
        Observable.Timer(TimeSpan.FromSeconds(time)).TakeUntilDisable(gameObject).Subscribe(x =>
        {
            EndGameStatus endGameStatus = new EndGameStatus
            {
                addedCoins = 0,
                win = false
            };
            LevelManager.Instance.EndLevel(endGameStatus);
        });
    }
}
