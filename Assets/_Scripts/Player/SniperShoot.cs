using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class SniperShoot : MonoBehaviour
{
    public Shooting shooting;

    [SerializeField] private GameObject aim;
    [SerializeField] private GameObject gameplayUI;
    [SerializeField] private RectTransform aimTransform;
    [SerializeField] private Hands hands;
    [SerializeField] private float cameraSensitive = 0.2f;
    [SerializeField] private LayerMask bulletMask;

    private float delayShot = 1.5f;

    private bool aimActive = false;
    private float delay;
    private float aimDelayShoot;

    private Vector3 target = Vector3.zero;
    private Vector2 centerScreen;
    private float defaultFieldOfView;
    private float aimFieldOfView = 15;
    private Camera cam;
    private float xAngle;
    private float yAngle;
    private float xAngleTemp;
    private float yAngleTemp;
    private Vector2 firstPoint;
    private Vector2 secondPoint;
    private bool isShoot;
    private bool isStart;

    private bool isRecoil;
    private bool isRecoilStart;
    private bool isRecoilBack;
    private Vector3 cameraRecoilStart;
    private Vector3 cameraRecoilTemp;
    private bool isZommingBack = false;

    private IDisposable shootAimBackDis;

    void Start()
    {
        isStart = false;
        isRecoil = false;
        isRecoilBack = false;
        aimDelayShoot = 0;
        delay = 0;
        aimActive = false;
        cam = Camera.main;
        defaultFieldOfView = cam.fieldOfView;
        centerScreen = new Vector2(-Screen.width / 2, -Screen.height / 2);
        EventManager.OnStartGame.Subscribe(() => isStart = true);
        isShoot = false;
    }

    void Update()
    {
        if (!isStart) return;

        delay -= Time.deltaTime;

        if (isZommingBack) return;

        if ((isShoot|| isRecoil) && Input.GetMouseButtonDown(0))
        {
            shootAimBackDis.Dispose();
            isShoot = false;
            firstPoint = Input.mousePosition;
            xAngleTemp = xAngle;
            yAngleTemp = yAngle;
        }
        //������
        if (isRecoil)
        {
            if(isRecoilStart)
            {
                cameraRecoilStart = cam.transform.localRotation.eulerAngles;// = Quaternion.Euler(yAngle * cameraSensitive * 2, xAngle * cameraSensitive, 0.0f);
                cameraRecoilTemp = cam.transform.localRotation.eulerAngles;
                isRecoilStart = false;
            }
            if (isRecoilBack)
            {
                cameraRecoilTemp.x += Time.deltaTime * 5f;
                if (cameraRecoilTemp.x - cameraRecoilStart.x > 0)
                {
                    isRecoilBack = false;
                    isRecoil = false;
                    cam.transform.localRotation = Quaternion.Euler(cameraRecoilStart);
                    return;
                }
            }
            else
                cameraRecoilTemp.x -= Time.deltaTime * 8f;
            
            cam.transform.localRotation = Quaternion.Euler(cameraRecoilTemp);
            if (cameraRecoilTemp.x - cameraRecoilStart.x < -1.5f  && !isRecoilBack)
                isRecoilBack = true;
            return;
        }
        if (delay > 0)
            return;

        Vector2 clickPosition = Input.mousePosition;

        if (Input.GetMouseButton(0))
        {
            if (!aimActive)
            {
                ActiveAim(true);
              //  cam.fieldOfView = aimFieldOfView;
                xAngle = cam.transform.localRotation.eulerAngles.x;
                yAngle = cam.transform.localRotation.eulerAngles.y;
                firstPoint = Input.mousePosition;
                xAngleTemp = xAngle;
                yAngleTemp = yAngle;
            }

            if(aimActive)
            {
                // �������� ������
                secondPoint = (Vector2)Input.mousePosition;
                xAngle = xAngleTemp + (secondPoint.x - firstPoint.x) * 180 / Screen.width;
                yAngle = yAngleTemp - (secondPoint.y - firstPoint.y) * 90 / Screen.height;
                cam.transform.localRotation = Quaternion.Euler(yAngle * cameraSensitive * 2, xAngle * cameraSensitive, 0.0f);
                aimDelayShoot += Time.deltaTime;
            }
            Ray ray = new Ray(cam.transform.position, cam.transform.forward);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit,1000, ~bulletMask))
            {
                target = hit.point;
            }
            else
            {
                target = ray.direction * 1000 + Camera.main.transform.position;
            }
            shooting.hands.currentWeapon.transform.LookAt(target);
        }

        if (Input.GetMouseButtonUp(0))
        {
            
            if (aimDelayShoot > 0.7f)
            {
                Ray ray = new Ray(cam.transform.position, cam.transform.forward);
                Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 5);
                shooting.Shoot(target, true);
                isRecoil = true;
                isRecoilStart = true;
                isShoot = true;
                shootAimBackDis = Observable.Timer(0.6f.sec()).Subscribe(z => ActiveAim(false));
            }
            //   cam.fieldOfView = defaultFieldOfView;
            if (!isShoot)
                ActiveAim(false);

            //shooting.hands.currentWeapon.transform.localRotation = Quaternion.Euler(Vector3.zero);
            aimDelayShoot = 0;
        }
    }

    IDisposable aimDispose;

    private void ActiveAim(bool enable)
    {
        Observable.EveryUpdate().TakeWhile(x => isRecoil).TakeUntilDisable(gameObject).Finally(() =>
        {
            if (enable == true)
            {
                aimActive = enable;
                aim.SetActive(enable);
                hands.currentWeaponObj.SetActive(!enable);
                gameplayUI.SetActive(!enable);
            }

            float fieldOfView = enable ? aimFieldOfView : defaultFieldOfView;

            xAngleTemp = xAngle;
            yAngleTemp = yAngle;
            firstPoint = Input.mousePosition;

            if (enable == false)
            {
                if (isShoot)
                    delay = delayShot;
                isShoot = false;
                isZommingBack = true;
            }
            aimDispose?.Dispose();
            aimDispose = Observable.EveryUpdate().TakeUntilDisable(gameObject)
                .Finally(() =>
                {
                    if (enable == false)
                    {
                        aimActive = enable;
                        aim?.SetActive(enable);
                        hands.currentWeaponObj.SetActive(!enable);
                        gameplayUI.SetActive(!enable);
                        cam.transform.localRotation = Quaternion.Euler(Vector3.zero);
                        isZommingBack = false;
                    }

                })
                .TakeWhile(z => cam.fieldOfView != fieldOfView)
                .Subscribe(x =>
                {
                    cam.fieldOfView = Mathf.MoveTowards(cam.fieldOfView, fieldOfView, 75f * Time.deltaTime);
                });
        }).Subscribe();
        
    }

    private void OnDisable()
    {
        cam.transform.localRotation = Quaternion.Euler(Vector3.zero);
        cam.fieldOfView = defaultFieldOfView;
    }
}
