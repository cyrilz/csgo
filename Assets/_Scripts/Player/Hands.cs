using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hands : MonoBehaviour
{
    public GameObject currentWeaponObj;
    public GameObject currentShieldObj;

    public Transform weaponPlaceHolder;
    public Transform shieldPlaceHolder;

    [HideInInspector] public Weapon currentWeapon;
    [HideInInspector] public Shield currentShield;

    private void Awake()
    {
       // shieldPlaceHolderLocalPosition = shieldPlaceHolder.localPosition;
        // shieldPlaceHolder.parent = null;

        if (currentWeaponObj != null)
        {
            SetNewWeapon(currentWeaponObj);
        }
    }

    public void SetNewWeapon(GameObject newWeapon)
    {
        if (newWeapon.activeInHierarchy)
        {
            Destroy(currentWeaponObj);
        }
        currentWeaponObj = Instantiate(newWeapon, weaponPlaceHolder.position, weaponPlaceHolder.rotation, weaponPlaceHolder);
        currentWeapon = currentWeaponObj.GetComponent<Weapon>();
    }

    public void SetNewShield(GameObject newShield)
    {
        if (newShield.activeInHierarchy)
        {
            Destroy(currentShieldObj);
        }
        currentShieldObj = Instantiate(newShield, shieldPlaceHolder.position, shieldPlaceHolder.rotation, shieldPlaceHolder);
        currentShield = currentShieldObj.transform.GetComponentInChildren<Shield>();
    }
    //35

    public void ThrowWeapon()
    {
        if (currentWeapon.GetComponent<Weapon>() != null)
        {
            currentWeapon.GetComponent<Weapon>().DeactivateHands();
            currentWeaponObj.GetComponent<BoxCollider>().enabled = true;
            currentWeaponObj.GetComponent<Rigidbody>().useGravity = true;
            currentWeaponObj.GetComponent<Rigidbody>().AddRelativeForce(0f, 0f, 300f);
        }

        currentWeaponObj.transform.parent = null;

        if (currentWeaponObj.GetComponent<Animator>() != null)
        {
            currentWeaponObj.GetComponent<Animator>().enabled = false;
        }

        currentWeaponObj = null;
        currentWeapon = null;
    }

    public GameObject GetCurrentWeapon() => currentWeapon.gameObject;
}
