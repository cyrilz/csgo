using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class HostageDeath : HealthPoints
{
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject model;

    public Rigidbody[] rigidbodies;

    public override void GetHit(Collider collider, Vector3 force, float time = 0, bool showHitMarker = true) 
    {
        animator.enabled = false;

        foreach (Rigidbody rigidbody in rigidbodies) 
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.useGravity = true;
            rigidbody.isKinematic = false;
        }

        collider.GetComponent<Rigidbody>()?.AddForce(force);
        
        animator.transform.parent = null;

        Destroy(gameObject);
    }

    public void StandUp()
    {
        animator.SetBool("StandUp", true);
    }

    public void IdledUp()
    {
        animator.Play("Hostage_Up", 0, 1f);
        Vector3 tempPos = model.transform.localPosition;
        tempPos.y = -0.68f;
        model.transform.localPosition = tempPos;
    }

}
