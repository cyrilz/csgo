using UnityEngine;
using UnityEditor;
using System.Linq;

[CustomEditor(typeof(HostageDeath))]
public class HostageDeathEditor : Editor
{
    HostageDeath hostageDeath;

    Collider collider;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        hostageDeath = (HostageDeath)target;

        collider = hostageDeath.GetComponentInChildren<Collider>();

        if (GUILayout.Button("Set Up All Rigidbodies"))
        {
            hostageDeath.rigidbodies = hostageDeath.transform.GetComponentsInChildren<Rigidbody>();

            foreach (Rigidbody rigidbody in hostageDeath.rigidbodies) {
                rigidbody.useGravity = false;
                rigidbody.isKinematic = true;
            }

            Debug.Log("Rigidbodies found: " + hostageDeath.rigidbodies.Length);
        }
    }
}